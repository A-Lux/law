@extends('layouts.main')

@section('content')

    <div class="promotional">
        <div class="codes" id="codes">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-4 offset-md-5"></div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-5 col-12">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-personal-tab" data-toggle="pill" href="#v-pills-personal" data-title="Персональные данные">Персональные данные</a>
                            <a class="nav-link" id="v-pills-password-tab" data-toggle="pill" href="#v-pills-password">Смена пароля</a>
                            <a class="nav-link" id="v-pills-shops-tab" data-toggle="pill" href="#v-pills-shops" data-title="Мои покупки">Мои покупки</a>
                            <a class="nav-link" id="v-pills-question-tab" data-toggle="pill" href="#v-pills-question" role="tab" aria-controls="v-pills-question" aria-selected="false"  data-title="Задать вопрос юристу">Задать вопрос юристу</a>
                            <!-- <a class="nav-link" id="v-pills-bonus-tab" data-toggle="pill" href="#v-pills-bonus" role="tab" aria-controls="v-pills-bonus" aria-selected="false" data-title="Бонусы">Бонусы</a>
                            <a class="nav-link" id="v-pills-codes-tab" data-toggle="pill" href="#v-pills-codes" role="tab" aria-controls="v-pills-codes" aria-selected="false" data-title="Промокоды">Промокоды</a> -->
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-7 col-12">
                    <div class="col-lg-10 col-md-7">
                        <h1 class="title"></h1>
                    </div>
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade" id="v-pills-shops" role="tabpanel" aria-labelledby="v-pills-shops-tab">
                                <div class="tab-container">

                                    @if($buys)
                                        @foreach($buys as $buy)
                                            <h2> {{ $buy['name'] }}</h2>
                                            <table>
                                                <tbody>
                                                    @foreach($buy['items'] as $item)
                                                        <tr>
                                                            <td>
                                                            <span>{{ $item->name }}
                                                                @if(!empty(json_decode($item->file)))
                                                                    <a href="{{ 'storage/' . json_decode($item->file)[0]->download_link }}">Скачать</a>
                                                                @endif
                                                            </span>
                                                            </td>
                                                            <td>
                                                            <span>
                                                                <b class="d-block text-center">
                                                                    {{ $item->count }}
                                                                </b>
                                                            </span>
                                                            </td>
                                                            <td>
                                                            <span>
                                                                <b>{{ $item->render }}</b> (Оказанных услуг)
                                                            </span>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-question" role="tabpanel" aria-labelledby="v-pills-question-tab">
                                <div class="question-content">
                                    <form name="questionLawyer" id="question-lawyer">
                                        <div class="dropdown">
                                            <select name="service" class="btn question-button dropdown-toggle" id="dropdownMenuButton">
                                                @if(isset($consultations))
                                                    @foreach($consultations as $consultation)
                                                        <option class="dropdown-item consultation"
                                                                data-id="{{ $consultation['id'] }}"
                                                           value="{{ $consultation['service_id'] }}"
                                                        >
                                                            {{ $consultation['name'] }}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="questions-themes">
                                            <input type="text" name="theme" placeholder="Тема вопроса">
                                        </div>
                                        <div class="question-info">
                                            <textarea name="question" id="" cols="30" rows="7" placeholder="Вопрос"></textarea>
                                        </div>
                                        <div class="question-send">
                                            <button type="submit" class="question-button">
                                                Отправить вопрос
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane fade active show" id="v-pills-personal" role="tabpanel" aria-labelledby="v-pills-personal">
                                <div class="row">
                                    <div class="col-xl-4">
                                        <div class="personal-title">
                                            <h1>Имя</h1>
                                            <h1>Ваш телефон</h1>
                                            <h1>Ваш e-mail</h1>
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="personal-info">
                                            <p>Admin</p>
                                            <p>+74324361533</p>
                                            <p>admin@admin.com</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
