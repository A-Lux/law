@extends('layouts.main')

@section('content')
    <div class="brochures">
        <div class="container py-md-0 py-5">
            <div class="row my-5">
                <div class="col-lg-12">
                    <h1 class="title">Брошюры</h1>
                </div>

                <div class="col-lg-4 col-sm-6 col-12 my-4 aos-init aos-animate" data-aos="fade-up">
                    <a class="brochure-link" href="link_to_pdf.pdf">
                        <div class="brochure">
                            <figure>
                                <img src="http://law.loc/storage/package-services\May2020\tNojpN8g37ntBn8JtXtM.png">
                            </figure>
                            <p class="description">Пакет Halyk  для физических лиц</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6 col-12 my-4 aos-init aos-animate" data-aos="fade-up">
                    <a class="brochure-link" href="link_to_pdf.pdf">
                        <div class="brochure">
                            <figure>
                                <img src="http://law.loc/storage/package-services\May2020\tNojpN8g37ntBn8JtXtM.png">
                            </figure>
                            <p class="description">Пакет Halyk  для физических лиц</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6 col-12 my-4 aos-init aos-animate" data-aos="fade-up">
                    <a class="brochure-link" href="link_to_pdf.pdf">
                        <div class="brochure">
                            <figure>
                                <img src="http://law.loc/storage/package-services\May2020\tNojpN8g37ntBn8JtXtM.png">
                            </figure>
                            <p class="description">Пакет Halyk  для физических лиц</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6 col-12 my-4 aos-init aos-animate" data-aos="fade-up">
                    <a class="brochure-link" href="link_to_pdf.pdf">
                        <div class="brochure">
                            <figure>
                                <img src="http://law.loc/storage/package-services\May2020\tNojpN8g37ntBn8JtXtM.png">
                            </figure>
                            <p class="description">Пакет Halyk  для физических лиц</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection