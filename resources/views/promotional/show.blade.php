@extends('layouts.main')

@section('content')

    <div class="promotional-inner">
        @if(!$banners->isEmpty())
            <div class="slider-wrapper">
                <div class="container over py-5">
                    <div class="row py-5">
                        <div class="col-lg-1 d-flex align-items-center justify-content-center"><i class="prev-btn fal fa-chevron-left"></i></div>
                        <div class="offset-lg-7 offset-md-6"></div>
                        <div class="col-lg-3 col-md-6">
                            <div class="request-block">
                                <img src="dist/images/comment-alt-lines.png" class="comment">
                                <h1 class="title">Вы можете <span>прямо сейчас</span> проконсультироваться</h1>
                                <p class="description">Оставьте свой номер телефона и мы вам перезвоним</p>
                                <form id="request-user" class="send-request" name="requestUser">
                                    <div class="form-group">
                                        <input type="text" name="name" placeholder="Ваше имя" class="name" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="phone" placeholder="Введите ваш телефон" class="phone" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="send">Оставить заявку</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-1 d-flex align-items-center justify-content-center"><i class="next-btn fal fa-chevron-right"></i></div>
                    </div>
                </div>
                <div class="main-slider slider">
                    @foreach($banners as $banner)
                        <div>
                            <div class="item" style="background-image: url({{ asset('storage/' . str_replace('\\','/', $banner->image)) }})">
                                <div class="container py-5">
                                    <div class="row py-5">
                                        <div class="offset-lg-1"></div>
                                        <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                            <h1 class="title">{{ $banner->title }}</h1>
                                            <p class="description">{{ $banner->content }}</p>
                                            @if($banner->statusButton == 1)
                                                <a href="{{ $banner->link }}" class="check-details">Посмотреть подробнее</a>
                                            @endif
                                        </div>
                                        <div class="offset-lg-5 offset-md-6">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <a href="#inner" class="anchor">
                    <i class="fal fa-anchor"></i>
                </a>
            </div>
        @endif
        <div id="inner">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-2"></div>
                    <div class="col-lg-8">
                        <h1 class="title">
                            {!! $package->title !!}
                        </h1>
                    </div>
                    <div class="offset-lg-2"></div>
                </div>
                <div class="row">
                    <div class="col-lg-12 mb-5">
                        @if($package->status_services == 1)
                            <table>
                                <thead>
                                    <tr>
                                        <th style="width: 5%"><span>№</span></th>
                                        <th><span>Наименование услуги</span></th>
                                        <th style="width: 25%"><span>Количество</span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($services as $service)
                                        <tr>
                                            <td>
                                                <span><b>{{ $loop->iteration }}</b></span>
                                            </td>
                                            <td>
                                                <span>{{ $service->name }}</span>
                                                @if(!empty($service->content))
                                                    {!! $service->content !!}
                                                @endif
                                            </td>
                                            <td>
                                                @if($service->total == 0)
                                                    @php $total = 'включено'; @endphp
                                                @else
                                                    @php $total = $service->total; @endphp
                                                @endif
                                                <span><b>{{ $total }}</b></span>
                                            </td>
                                        </tr>

                                        @php $count = $loop->iteration; @endphp
                                    @endforeach
                                    <tr>
                                        <td>
                                            <span><b>{{ $count+1 }}</b></span>
                                        </td>
                                        <td>
                                            <span>Срок действия пакета</span>
                                        </td>
                                        <td>
                                            <span><b>{{ $package->year }}</b></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span><b>{{ $count+2 }}</b></span>
                                        </td>
                                        <td>
                                            <span>Стоимость пакета</span>
                                        </td>
                                        <td>
                                            <span><b>{{ $package->price }} тенге</b></span>
                                        </td>
                                    </tr>
                                    @if(!empty($package->sale))
                                        <tr>
                                            <td colspan="3" style="width: 100%"><h3>Скидки</h3></td>
                                        </tr>
                                        <tr>
                                            <td><span><b>{{ $count+3 }}</b></span></td>
                                            <td><span>Скидка на дополнительные юридические услуги</span></td>
                                            <td><span><b>{{ $package->sale }} %</b></span></td>
                                        </tr>
                                    @endif
                                    @if(!empty($package->bonus))
                                        <tr>
                                            <td colspan="3" style="width: 100%"><h3>Партнерская программа</h3></td>
                                        </tr>
                                        <tr>
                                            <td><span><b>{{ $count+4 }}</b></span></td>
                                            <td><span>За реализацию пакета Halyk бонус составляет:</span></td>
                                            <td><span><b>{{ $package->bonus }}</b></span></td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        @endif
                        @if($package->price != 0)
                            <form style="display: contents;" id="purchaseForm" data-user="{{ Auth::user() ? 1 : 0 }}"><table>
                                <thead>
                                    <tr>
                                        <th><span>Наименование услуги</span></th>
                                        <th><span>Цена</span></th>
                                        <th><span>Количество</span></th>
                                        <th><span>Сумма</span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><span id="packageName" data-id="{!! $package->id !!}">{!! $package->title !!}</span></td>
                                        <td><span data-price="{!! $package->price !!}" id="current_price">{!! $package->price !!} тг</span></td>
                                        <td>
                                            <div class="quantity-controls">
                                                <button type="button" class="decrease">-</button>
                                                <input id="quantity" disabled type="number" value="1" />
                                                <button type="button" class="increase">+</button>
                                                </div>
                                        </td>
                                        <td>
                                            <span>
                                                <span data-sum="{!! $package->price !!}" id="sum">{!! $package->price !!} тг</span>
                                            </span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <button type="submit" class="purchase" >Купить</button>
                            </form>
                        @endif
                    </div>
                    <div class="col-lg-12 mb-5">
                        <div class="about-product">
                            {!! $package->content !!}
                        </div>
                    </div>
                    <div class="offset-lg-2"></div>
                    <div class="col-lg-8">
                        <div class="callback">
                            <h2>Заполните форму обратной связи</h2>
                            <form id="callback-user" class="callback-form" name="callbackUser">
                                <div class="form-group">
                                    <input type="text" name="name" placeholder="Введите имя" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="phone" placeholder="Введите телефон" required>
                                </div>
                                <div class="form-group">
                                    <textarea placeholder="Введите текст сообщения" name="message" rows="5"></textarea>
                                </div>
                                <div class="form-group">
                                    <button class="callback-btn" type="submit">Отправить</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="offset-lg-2"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
