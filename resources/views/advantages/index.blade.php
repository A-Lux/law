@extends('layouts.main')

@section('content')

    <div class="advantages">
        @if(!$banners->isEmpty())
            <div class="slider-wrapper">
                <div class="container over py-5">
                    <div class="row py-5">
                        <div class="col-lg-1 d-flex align-items-center justify-content-center"><i class="prev-btn fal fa-chevron-left"></i></div>
                        <div class="offset-lg-7 offset-md-6"></div>
                        <div class="col-lg-3 col-md-6">
                            <div class="request-block">
                                <img src="dist/images/comment-alt-lines.png" class="comment">
                                <h1 class="title">Вы можете <span>прямо сейчас</span> проконсультироваться</h1>
                                <p class="description">Оставьте свой номер телефона и мы вам перезвоним</p>
                                <form id="request-user" class="send-request" name="requestUser">
                                    <div class="form-group">
                                        <input type="text" name="name" placeholder="Ваше имя" class="name" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="phone" placeholder="Введите ваш телефон" class="phone" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="send">Оставить заявку</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-1 d-flex align-items-center justify-content-center"><i class="next-btn fal fa-chevron-right"></i></div>
                    </div>
                </div>
                <div class="main-slider slider">
                    @foreach($banners as $banner)
                        <div>
                            <div class="item" style="background-image: url({{ asset('storage/' . str_replace('\\','/', $banner->image)) }})">
                                <div class="container py-5">
                                    <div class="row py-5">
                                        <div class="offset-lg-1"></div>
                                        <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                            <h1 class="title">{{ $banner->title }}</h1>
                                            <p class="description">{{ $banner->content }}</p>
                                            @if($banner->statusButton == 1)
                                                <a href="{{ $banner->link }}" class="check-details">Посмотреть подробнее</a>
                                            @endif
                                        </div>
                                        <div class="offset-lg-5 offset-md-6">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <a href="#our-advantages" class="anchor">
                    <i class="fal fa-anchor"></i>
                </a>
            </div>
        @endif
        <div class="our-advantages" id="our-advantages">
            <div class="container">
                <div class="row my-5">
                    <div class="col-lg-12">
                        <h1 class="title">{{ $advantage->title }}</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="description">
                                {!! $advantage->text !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
