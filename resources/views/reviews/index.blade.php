@extends('layouts.main')

@section('content')

    <div class="reviews">
        <div class="container">
            <div class="row py-5">
                <div class="col-lg-12">
                    <h1 class="title">Отзывы</h1>
                </div>
            </div>
        </div>
        <div class="reviews-slider slider">
            @foreach($reviews as $review)
                <div>
                    <div class="item">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-3 col-12 d-flex flex-column flex-md-column flex-sm-row align-items-md-start align-items-center justify-content-between">
                                    <h2 class="initials">
                                        {{ $review->username }}
                                    </h2>
                                </div>
                                <div class="col-lg-9 p-0 col-12 d-flex flex-column justify-content-between align-items-start">
                                    <div class="message__wrapper">
                                        <div class="star-rating__wrapper" data-star="5">
                                            <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating">
                                            </label>
                                            <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating">
                                            </label>
                                            <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating">
                                            </label>
                                            <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating">
                                            </label>
                                            <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating">
                                            </label>
                                        </div>
                                        <p class="message">
                                            {{ $review->content }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="container">
            <div class="row py-sm-5 my-sm-5">
                <div class="offset-lg-4 offset-sm-2"></div>
                <div class="col-lg-4 col-sm-8">
                    <a href="#" class="leave-review">Оставить отзыв</a>
                </div>
                <div class="offset-lg-4 offset-sm-2"></div>
            </div>
        </div>
    </div>
@endsection
