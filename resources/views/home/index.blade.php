@extends('layouts.main')

@section('content')

    <div class="main">
        @if(!$banners->isEmpty())
            <div class="slider-wrapper">
                <div class="container over py-5">
                    <div class="row py-5">
                        <div class="col-lg-1 d-flex align-items-center justify-content-center"><i class="prev-btn fal fa-chevron-left"></i></div>
                        <div class="offset-lg-7 offset-md-6"></div>
                        <div class="col-lg-3 col-md-6">
                            <div class="request-block">
                                <img src="dist/images/comment-alt-lines.png" class="comment">
                                <h1 class="title">Вы можете <span>прямо сейчас</span> проконсультироваться</h1>
                                <p class="description">Оставьте свой номер телефона и мы вам перезвоним</p>
                                <form id="request-user" class="send-request" name="requestUser">
                                    <div class="form-group">
                                        <input type="text" name="name" placeholder="Ваше имя" class="name" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="phone" placeholder="Введите ваш телефон" class="phone" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="send">Оставить заявку</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-1 d-flex align-items-center justify-content-center"><i class="next-btn fal fa-chevron-right"></i></div>
                    </div>
                </div>
                <div class="main-slider slider">
                    @foreach($banners as $banner)
                        <div>
                            <div class="item" style="background-image: url({{ asset('storage/' . str_replace('\\','/', $banner->image)) }})">
                                <div class="container py-5">
                                    <div class="row py-5">
                                        <div class="offset-lg-1"></div>
                                        <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                            <h1 class="title">{{ $banner->title }}</h1>
                                            <p class="description">{{ $banner->content }}</p>
                                            @if($banner->statusButton == 1)
                                                <a href="{{ $banner->link }}" class="check-details">Посмотреть подробнее</a>
                                            @endif
                                        </div>
                                        <div class="offset-lg-5 offset-md-6">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <a href="#services" class="anchor">
                    <i class="fal fa-anchor"></i>
                </a>
            </div>
        @endif
        <div id="services" class="services">
            <div class="container py-md-0 py-5">
                <div class="row my-5">
                    <div class="col-lg-12">
                        <h1 class="title">Наши услуги</h1>
                    </div>
                    @foreach($services as $service)
                        <div class="col-lg-4 col-sm-6 col-12 my-4" data-aos="fade-up">
                            <a class="service-link" href="/show/{{ $service->id }}">
                                <div class="service">
                                    <figure>
                                        <img src="{{ asset('storage/' . $service->image) }}">
                                    </figure>
                                    <p class="description">{{ $service->name }}</p>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="advantages">
            <div class="container">
                <div class="row py-5 my-5">
                    <div class="col-lg-6 col-md-7 col-12">
                        <h1 class="title">BUKVA ZAKONA Юридический онлайн сервис</h1>
                        <div class="description">
                            <p><b>Онлайн сервис "BukvaZakona" - это:</b></p>
                            <p>Современный подход дистанционного юридического обслуживания</p>
                            <p>Оперативная и доступная юридическая поддержка для населения и бизнеса</p>
                            <p>Экономия времени - удобно, быстро и эффективно, так как клиент имеет возможность получить квалифицированную правовую помощь, не выходя из дома, на работе или в путешествии</p>
                        </div>
                        <div class="reward-cards">
                            <div class="card-body">
                                <h1 class="title">
                                    более
                                    <span>150</span>
                                </h1>
                                <p class="description">
                                    Выигранных или частично выигранных дел
                                </p>
                            </div>
                            <div class="card-body">
                                <h1 class="title">
                                    более
                                    <span>100</span>
                                </h1>
                                <p class="description">
                                    Постоянных клиентов
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="offset-lg-3"></div>
                    <div class="col-lg-3 col-md-5 col-12 pt-4 pt-lg-0">
                        <div class="brochures-block">
                            <h1 class="title">Статьи и новости</h1>
                            <p class="description">на различные юридические темы с формами образцов документов</p>

                            @foreach($brochures as $brochure)
                            <a href="{{
                                empty(json_decode($brochure->file))
                                ? '#'
                                : asset('storage/' . json_decode($brochure->file)[0]->download_link)
                                }}" target="_blank" style="display: contents; text-decoration: none;">
                                <div class="brochure">
                                        <img src="{{ asset('storage/' . $brochure->image) }}">
                                    <h2 class="title">{{ $brochure->title }}</h2>
                                </div>
                            </a>
                            @endforeach
                            <a href="/brochures" class="more">Смотреть все статьи</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="reviews">
            <div class="container">
                <div class="row py-5">
                    <div class="col-lg-12">
                        <h1 class="title">Отзывы</h1>
                    </div>
                </div>
            </div>
            <div class="reviews-slider slider">
                @foreach($reviews as $review)
                    <div>
                        <div class="item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-3 col-12 d-flex flex-column flex-md-column flex-sm-row align-items-md-start align-items-center justify-content-between">
                                        <h2 class="initials">
                                            {{ $review->username }}
                                        </h2>
                                    </div>
                                    <div class="col-lg-9 p-0 col-12 d-flex flex-column justify-content-between align-items-start">
                                        <div class="message__wrapper">
                                            <div class="star-rating__wrapper" data-star="5">
                                                <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                                    <input class="star-rating__input" type="radio" name="rating">
                                                </label>
                                                <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                                    <input class="star-rating__input" type="radio" name="rating">
                                                </label>
                                                <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                                    <input class="star-rating__input" type="radio" name="rating">
                                                </label>
                                                <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                                    <input class="star-rating__input" type="radio" name="rating">
                                                </label>
                                                <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                                    <input class="star-rating__input" type="radio" name="rating">
                                                </label>
                                            </div>
                                            <p class="message">
                                                {{ $review->content }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="container">
                <div class="row py-sm-5 my-sm-5">
                    <div class="offset-lg-4 offset-sm-2"></div>
                    <div class="col-lg-4 col-sm-8">
                        <a href="/reviews" class="more">Читать все отзывы</a>
                    </div>
                    <div class="offset-lg-4 offset-sm-2"></div>
                </div>
            </div>
        </div>
        <div class="video">
            <div class="container">
                <div class="row py-5">
                    <div class="col-lg-12 d-flex justify-content-center">
                        <figure class="video-wrapper">
                            <img src="dist/images/tv.png">
                            <div class="fluid-width-video-wrapper">
                                @php
                                    $video = json_decode($about->video);
                                    $video = isset($video[0])
                                                ? $video[0]->download_link
                                                : '';
                                @endphp
                                <video loop="" playsinline="" poster="dist/images/poster.png">
                                    <source src="{{ asset('storage/' . $video) }}" type="video/mp4" codecs="avc1.42E01E, mp4a.40.2">
                                </video>
                                <button type="button" id="play-btn">
                                    <div class="first-circle"></div>
                                    <div class="second-circle"></div>
                                </button>
                            </div>
                            <figcaption>{!! $about->subtitle !!}</figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
