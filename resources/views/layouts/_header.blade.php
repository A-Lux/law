<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no" />
    <title>{{ setting('site.title') }}</title>
    <meta name="metaDesc" content="{{ setting('site.description') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/nprogress.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/magnific-popup.css') }}">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js" integrity="sha256-bd8XIKzrtyJ1O5Sh3Xp3GiuMIzWC42ZekvrMMD4GxRg=" crossorigin="anonymous"></script>
</head>
<body>
<div class="header">
    <div class="container">
        <div class="row py-4">
            <div class="col-lg-2 col-md-4 col-6 mb-4 mb-lg-0">
                <a href="/" class="logo">
                    <img src="{{ asset('storage/' . setting('site.logo')) }}">
                </a>
            </div>
            <div class="col-lg-5 col-md-8 col-6 mb-4 mb-lg-0">
                <a href="" class="toggle">
                    <span></span>
                </a>
                <nav class="main-nav">
                    <ul>
                        @foreach(menu('site', '_json') as $menuItem)
                            <li><a href="{{$menuItem->url}}">{{ $menuItem->title }}</a></li>
                        @endforeach
                    </ul>
                </nav>
            </div>
            <div class="col-lg-3 col-6 d-flex flex-lg-row flex-wrap justify-content-between align-items-lg-center">
                @guest
                    <a  href="#nav-registration" data-toggle="modal" data-target="#modal" class="registration-btn modal-toggle">Зарегистрироваться</a>
                    <a  href="#nav-authorization" data-toggle="modal" data-target="#modal" class="authorization-btn modal-toggle">Войти</a>
                @endguest
                @auth
                    <nav class="main-nav">
                        <ul>
                            <li>
                                <a href='/profile'>Личный кабинет</a>
                            </li>
                        </ul>
                    </nav>

                    <form method="post" action="/logout">
                        @csrf
                        <button class="authorization-btn">Выйти</button>
                    </form>
                @endauth
            </div>
            <div class="col-lg-2 col-6 d-md-flex flex-md-column flex-md-wrap justify-content-md-center align-items-md-end">
                <a href="tel: {{ setting('site.phone') }}" class="phone-number">{{ setting('site.phone') }}</a>
                <a href="#" class="callback-btn">Заказать обратный звонок</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                  <a class="nav-item nav-link" id="nav-registration-tab" data-toggle="tab" href="#nav-registration" role="tab" aria-controls="nav-registration" aria-selected="true">Регистрация</a>
                  <a class="nav-item nav-link active" id="nav-authorization-tab" data-toggle="tab" href="#nav-authorization" role="tab" aria-controls="nav-authorization" aria-selected="false">Войти</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade" id="nav-registration" role="tabpanel" aria-labelledby="nav-registration-tab">
                    <form id="registration-form" method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group">
                            <label>
                                <span>Ваше имя<sup>*</sup></span>
                                <input id="name" name="name" type="text" placeholder="Иван Иванов" class="@error('name') is-invalid @enderror"
                                       value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </label>
                        </div>
                        <div class="form-group">
                            <label>
                                <span>Ваш телефон<sup>*</sup></span>
                                <input id="phone" name="phone" type="text" placeholder="+7 (234) 123-45-67" class="@error('phone') is-invalid @enderror"
                                       required autocomplete="phone">

                                @error('phone')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </label>
                        </div>
                        <div class="form-group">
                            <label>
                                <span>Ваш e-mail<sup>*</sup></span>
                                <input id="email" name="email" type="email" placeholder="info@company.com" class="@error('email') is-invalid @enderror"
                                       value="{{ old('email') }}" required autocomplete="email">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </label>
                        </div>
                        <div class="form-group">
                            <label>
                                <span>Пароль<sup>*</sup></span>
                                <input id="password" name="password" type="password" class="@error('password') is-invalid @enderror" required autocomplete="new-password">
                            </label>
                        </div>
                        <div class="form-group">
                            <label>
                                <span>Подтверждение<sup>*</sup></span>
                                <input id="password-confirm" type="password" name="password_confirmation" required autocomplete="new-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </label>
                        </div>
                        <div class="form-group custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customControlInline">
                            <label class="custom-control-label" for="customControlInline">Я согласен с условиями публичной оферты</label>
                        </div>
                        <div class="form-group d-flex justify-content-center">
                            <button id="reg-btn" class="disabled" type="submit">Регистрация</button>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade show active" id="nav-authorization" role="tabpanel" aria-labelledby="nav-authorization-tab">
                    <form id="authorization-form" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <label>
                                <span>Ваш e-mail</span>
                                <input id="email" name="email" type="email" placeholder="info@company.com" required autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </label>
                        </div>
                        <div class="form-group">
                            <label>
                                <span>Пароль</span>
                                <input id="password" name="password" type="password" required autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </label>
                        </div>
                        <div class="form-group d-flex justify-content-center">
                            <button id="auth-btn" type="submit">Войти</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@if (Session::has('success'))
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })

        Toast.fire({
            icon: 'success',
            title: '{{ Session::get('success') }}'
        })
    </script>
@endif
@if (Session::has('error'))
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })

        Toast.fire({
            icon: 'error',
            title: '{{ Session::get('error') }}'
        })
    </script>
@endif
@if (Session::has('warning'))
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })

        Toast.fire({
            icon: 'warning',
            title: '{{ Session::get('warning') }}'
        })
    </script>
@endif
