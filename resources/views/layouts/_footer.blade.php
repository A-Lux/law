<div class="footer">
    <div class="container">
        <div class="row py-4">
            <div class="col-lg-2 col-md-3 col-sm-6 col-12 order-sm-0">
                <a href="/" class="logo">
                    <img src="{{ asset('storage/' . setting('site.footerLogo')) }}">
                </a>
            </div>
            <div class="col-lg-2 col-md-3 col-12 d-flex justify-content-sm-end justify-content-md-start align-items-lg-center order-sm-1">
                <ul class="map">
                    <li>
                        <a href="{{ empty(json_decode($documents['public_offer']->file))
                                    ? '#'
                                    : asset('storage/' . json_decode($documents['public_offer']->file)[0]->download_link) }}"
                            target="_blank">
                            {{  $documents['public_offer']->text}}
                        </a>
                    </li>
                    <li>
                        <a href="{{
                                    empty(json_decode($documents['partnership_agreement']->file))
                                    ? '#'
                                    : asset('storage/' . json_decode($documents['partnership_agreement']->file)[0]->download_link) }}"
                           target="_blank">
                            {{  $documents['partnership_agreement']->text}}
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-3 col-12 d-flex align-items-lg-center justify-content-md-start order-sm-2">
                <ul class="map">
                    <li><a href="#" data-toggle="modal" data-target="#footer-modal">Служба поддержки</a></li>
                    <li><a href="/reviews">Отзывы и предложения</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-12 col-sm-6 col-12 d-flex flex-column flex-wrap justify-content-sm-center align-items-sm-end order-sm-0 order-md-last order-lg-3 order-last">
                <a href="tel: {{ setting('site.phone') }}" class="phone-number">{{ setting('site.phone') }}</a>
{{--                <a href="mailto: {{ setting('site.mailerRequest') }}" class="e-mail">{{ setting('site.mailerRequest') }}</a>--}}
            </div>
            <div class="col-lg-2 col-md-3 col-12 d-flex justify-content-sm-end justify-content-md-start align-items-lg-center order-sm-4">
{{--                <p class="address">{!! setting('site.address') !!}</p>--}}
            </div>
            <div class="offset-lg-1">

            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-12 order-last order-md-0">
                <p class="copyright">
                    Все права защищены<br>
                    (с) Юридическая компания „BUKVA ZAKONA“, 2020
                </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-8 col-6 d-lg-flex">
                <p class="socials-title">
                    Подписывайтесь на нас в соцсетях:
                </p>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                <ul class="socials-map">
                    @foreach($socials as $social)
                        <li>
                            <a href="{{ $social->link }}">
                                {!! $social->image !!}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-lg-3 col-md-2 col-sm-12 col-12">
                <p class="developed-by">Разработано в <a href="https://a-lux.kz"><img src="dist/images/a-lux.png" alt=""></a></p>
            </div>
        </div>
    </div>
    <div class="modal fade" id="footer-modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-body">
                <form id="support-service" name="supportService">
                    @csrf
                    <div class="form-group">
                        <p>Свяжитесь с нами по номеру: <a href="tel: {{ setting('site.phone') }}">{{ setting('site.phone') }}</a></p>
                    </div>
                    <div class="form-group">
                        <label>
                            <span>Ваше имя<sup>*</sup></span>
                            <input name="name" type="text" placeholder="Иван Иванов" required>
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            <span>Ваш телефон<sup>*</sup></span>
                            <input name="phone" type="text" placeholder="+7 (234) 123-45-67" required>
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            <span>Сообщение</span>
                            <textarea name="message" type="text"></textarea>
                        </label>
                    </div>
                    <div class="form-group d-flex justify-content-center">
                        <button type="submit">Отправить</button>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
</div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="{{ asset('js/request.js') }}"></script>
    <script src="{{ asset('js/question-lawyer.js') }}"></script>
    <script src="{{ asset('dist/js/main.js') }}"></script>

    @if($errors->any())
        <script>
            $('#modal').modal('show')
            $('#nav-registration-tab').click();
        </script>
    @endif
    @if(session()->has('unauthenticated'))
        <script>
            $('#modal').modal('show')
        </script>
    @endif
</body>
</html>
