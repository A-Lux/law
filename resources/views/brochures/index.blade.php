@extends('layouts.main')

@section('content')
    <div class="brochures">
        <div class="container py-md-0 py-5">
            <div class="row my-5">
                <div class="col-lg-12">
                    <h1 class="title">Брошюры</h1>
                </div>
                @foreach($brochures as $brochure)
                    <div class="col-lg-4 col-sm-6 col-12 my-4 aos-init aos-animate" data-aos="fade-up">
                        <a class="brochure-link" href="{{
                                    empty(json_decode($brochure->file))
                                    ? '#'
                                    : asset('storage/' . json_decode($brochure->file)[0]->download_link)
                                    }}" target="_blank">
                            <div class="brochure">
                                <figure>
                                    <img src="{{ asset('storage/' . $brochure->image) }}">
                                </figure>
                                <p class="description">{{ $brochure->title }}</p>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection