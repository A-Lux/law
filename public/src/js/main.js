window.$ = window.jQuery = require("jquery");
require("./jquery.magnific-popup.min.js");
const offcanvas = require("hc-offcanvas-nav");
import "long-press-event/dist/long-press-event.min.js";
import SVG from "@svgdotjs/svg.js/src/svg";
import slick from "slick-carousel";
import "jquery-mask-plugin";
import tabs from "bootstrap";
import service from "./service.js";
import axios from "axios";

const Swal = require("sweetalert2");
const AOS = require("aos");
let userData = {};
const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: toast => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    }
});
window.onload = function() {
    $(function() {
        AOS.init();
    });
    var lastScrollTop = 0;
    $(window).scroll(function(event) {
        var scrollPosition = window.scrollY;
        if (scrollPosition < 800 && $(".header").hasClass("banner--clone")) {
            $(".header").removeClass("banner--clone");
            $(".header + *").removeClass("stuck");
        }
        var showHeaderPosition = 800;
        var st = $(this).scrollTop();
        if (
            st > lastScrollTop &&
            scrollPosition > showHeaderPosition &&
            window.matchMedia("(min-width: 768px)").matches
        ) {
            if (!$(".header").hasClass(".banner--clone")) {
                $(".header").addClass("banner--clone");
            }
            if (!$(".header + *").hasClass(".stuck")) {
                $(".header + *").addClass("stuck");
            }
            if ($(".header").hasClass("banner--unstick")) {
                $(".header").removeClass("banner--unstick");
            }
            if (!$(".header").hasClass(".banner--stick")) {
                $(".header").addClass("banner--stick");
            }
        } else if (
            st < lastScrollTop &&
            scrollPosition > showHeaderPosition &&
            window.matchMedia("(min-width: 768px)").matches
        ) {
            if ($(".header").hasClass("banner--stick")) {
                $(".header").removeClass("banner--stick");
            }
            if (!$(".header").hasClass(".banner--unstick")) {
                $(".header").addClass("banner--unstick");
            }
        }
        if (
            st > lastScrollTop &&
            scrollPosition > showHeaderPosition &&
            window.matchMedia("(max-width: 768px)").matches
        ) {
            if (!$(".header").hasClass(".banner--clone")) {
                $(".header").addClass("banner--clone");
            }
            if (!$(".header + *").hasClass(".stuck")) {
                $(".header + *").addClass("stuck");
            }
            if ($(".header").hasClass("banner--stick")) {
                $(".header").removeClass("banner--stick");
            }
            if (!$(".header").hasClass(".banner--unstick")) {
                $(".header").addClass("banner--unstick");
            }
        } else if (
            st < lastScrollTop &&
            scrollPosition > showHeaderPosition &&
            window.matchMedia("(max-width: 768px)").matches
        ) {
            if ($(".header").hasClass("banner--unstick")) {
                $(".header").removeClass("banner--unstick");
            }
            if (!$(".header").hasClass(".banner--stick")) {
                $(".header").addClass("banner--stick");
            }
        }
        lastScrollTop = st;
    });
    let checkbox = document.getElementById("customControlInline");
    let regBtn = document.getElementById("reg-btn");
    checkbox.addEventListener("click", function() {
        if (this.checked) {
            regBtn.classList.remove("disabled");
        } else {
            regBtn.classList.add("disabled");
        }
    });

    $(".modal-toggle").click(function(e) {
        var tab = e.target.hash;
        $('.nav-tabs > a[href="' + tab + '"]').tab("show");
    });
    let $main_nav = $(".main-nav");
    let $toggle = $(".toggle");

    let defaultData = {
        maxWidth: 769,
        customToggle: $toggle,
        levelTitles: false,
        navTitle: "Меню",
        insertClose: false,
        labelBack: "Назад"
        // disableBody: false,
    };
    let Nav = $main_nav.hcOffcanvasNav(defaultData);
    function carouselMake() {
        if (!window.matchMedia("(max-width: 992px)").matches) {
            let items = document.querySelectorAll(".main-slider .item");
            let imageSources = [];
            items.forEach(image => {
                imageSources.push(
                    image.style.backgroundImage
                        .replace(/url\((['"])?(.*?)\1\)/gi, "$2")
                        .split(",")[0]
                );
            });
            let images = [];
            for (const source of imageSources) {
                let image = new Image();
                image.src = source;
                images.push(image);
            }
            items.forEach((item, index) => {
                item.style.height = `${images[index].height}px`;
            });
        }
        const $main_slider = $(".main-slider");
        $main_slider.slick({
            slidesToShow: 1,
            dots: true,
            swipeToSlide: true,
            fade: true,
            autoplay: true,
            pauseOnHover: true,
            autoplaySpeed: 2500,
            cssEase: "linear",
            nextArrow: $(".slider-wrapper .next-btn"),
            prevArrow: $(".slider-wrapper .prev-btn"),
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        dots: true,
                        arrows: false
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        dots: true,
                        arrows: false
                    }
                },
                {
                    breakpoint: 578,
                    settings: {
                        dots: false,
                        arrows: false
                    }
                }
            ]
        });
    }
    function dotsWrap(el) {
        let dots = el;
        let container = document.createElement("div");
        container.classList.add("container");
        let row = document.createElement("div");
        row.classList.add("row");
        let col = document.createElement("div");
        col.classList.add("col-lg-11");
        let offset = document.createElement("div");
        offset.classList.add("offset-lg-1");
        dots.parentNode.insertBefore(col, dots);
        col.appendChild(dots);
        row.appendChild(offset);
        col.parentNode.insertBefore(row, col);
        row.appendChild(col);
        row.parentNode.insertBefore(container, row);
        container.appendChild(row);
    }
    carouselMake();
    if ($("ul").is(".slick-dots")) {
        dotsWrap(document.querySelector(".main-slider .slick-dots"));
    }
    $('[name="phone"]').mask("+79999999999");

    function playerSetting() {
        let playSvg = SVG()
            .addTo("#play-btn")
            .viewbox(0, 0, 512, 512)
            .width("86")
            .height("86");
        playSvg.svg(
            `<defs><style></defs><path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm115.7 272l-176 101c-15.8 8.8-35.7-2.5-35.7-21V152c0-18.4 19.8-29.8 35.7-21l176 107c16.4 9.2 16.4 32.9 0 42z" class="fa-secondary"/><path d="M371.7 280l-176 101c-15.8 8.8-35.7-2.5-35.7-21V152c0-18.4 19.8-29.8 35.7-21l176 107c16.4 9.2 16.4 32.9 0 42z" class="fa-primary"/>`
        );
        let playBtn = document.getElementById("play-btn");
        let caption = document.querySelector(".video-wrapper figcaption");
        let vid = document.querySelector(".video video");
        vid.addEventListener("click", function() {
            caption.style.display = "none";
            if (this.paused) {
                this.play();
                playBtn.style.opacity = "0";
                playBtn.style.pointerEvents = "none";
            } else {
                this.pause();
                playBtn.style.opacity = "1";
                playBtn.style.pointerEvents = "all";
            }
        });
        playBtn.addEventListener("click", function() {
            caption.style.display = "none";
            if (vid.paused) {
                vid.play();
                this.style.opacity = "0";
                this.style.pointerEvents = "none";
            } else {
                vid.pause();
                this.style.opacity = "1";
                this.style.pointerEvents = "all";
            }
        });
    }

    if ($("div").is(".reviews-slider")) {
        const $reviews_slider = $(".reviews-slider");
        $reviews_slider.slick({
            dots: true,
            centerMode: true,
            pauseOnHover: true,
            centerPadding: "20px",
            slidesToShow: 3,
            nextArrow: '<i class="next-btn fal fa-chevron-right"></i>',
            prevArrow: '<i class="prev-btn fal fa-chevron-left"></i>',
            variableWidth: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        dots: true,
                        arrows: false
                    }
                },
                {
                    breakpoint: 578,
                    settings: {
                        slidesToShow: 1,
                        centerMode: false,
                        variableWidth: false,
                        centerPadding: "0",
                        dots: false,
                        arrows: false
                    }
                }
            ]
        });
        $reviews_slider.on("wheel", function(e) {
            e.preventDefault();

            if (e.originalEvent.deltaY > 0) {
                $(this).slick("slickNext");
            } else {
                $(this).slick("slickPrev");
            }
        });
        playerSetting();
    } else if ($("div").is(".about")) {
        playerSetting();
    } else if ($("div").is(".promotional")) {
        let activeLink = document.querySelector(".codes .nav-link.active");
        let links = this.document.querySelectorAll(".codes .nav-link");
        let title = document.querySelector(".codes .title");
        if (activeLink.hasAttribute("data-title")) {
            title.innerText = activeLink.getAttribute("data-title");
        }
        links.forEach(link =>
            link.addEventListener("click", function() {
                if (link.hasAttribute("data-title")) {
                    title.innerText = link.getAttribute("data-title");
                } else {
                    title.innerText = "";
                }
            })
        );

        let personal = document.getElementById("v-pills-personal-tab");
        let password = document.getElementById("v-pills-password-tab");
        personal.addEventListener("click", function() {
            service.getUserData().then(response => {
                userData = response.data;
                Swal.fire({
                    showCloseButton: false,
                    title: "Персональные данные",
                    confirmButtonText: "Подтвердить",
                    html: `
                    <form>
                        <label>Ваше имя<input id="initials" type="text" class="swal2-input" placeholder="Иван Иванов" value="${userData.name}"></label>
                        <label>Ваш телефон<input id="phone-number" type="text" class="swal2-input" placeholder="+7 (234) 123 45 67" value="${userData.phone}"></label>
                        <label>Ваш e-mail<input id="e-mail" type="text" class="swal2-input" placeholder="info@company.com" value="${userData.email}"></label>
                    </form>`,
                    preConfirm: () => {
                        let formData = new FormData();
                        formData.append(
                            "name",
                            document.getElementById("initials").value
                        );
                        formData.append(
                            "phone",
                            document.getElementById("phone-number").value
                        );
                        return formData;
                    }
                }).then(result => {
                    if (result.value) {
                        service.updateUserData(result.value).then(response => {
                            if (response.data.status) {
                                Toast.fire({
                                    icon: "success",
                                    title: `${response.data.message}`
                                });
                            } else {
                                Toast.fire({
                                    icon: "error",
                                    title: `${response.data.message}`
                                });
                            }
                        });
                    }
                });
            });
        });

        password.addEventListener("click", function() {
            Swal.fire({
                showCloseButton: false,
                title: "Смена пароля",
                confirmButtonText: "Подтвердить",
                html:
                    '<label>Старый пароль<input id="old-pwd" type="password" class="swal2-input"></label>' +
                    '<label>Новый пароль<input id="new-pwd" type="password" class="swal2-input"></label>' +
                    '<label>Новый пароль<input id="repeat-pwd" type="password" class="swal2-input"></label>',
                preConfirm: () => {
                    let formData = new FormData();
                    formData.append(
                        "password",
                        document.getElementById("old-pwd").value
                    );
                    formData.append(
                        "new_password",
                        document.getElementById("new-pwd").value
                    );
                    formData.append(
                        "new_password_confirmation",
                        document.getElementById("repeat-pwd").value
                    );
                    return formData;
                }
            }).then(result => {
                if (result.value) {
                    service.updateUserPassword(result.value).then(response => {
                        console.log(response);
                        if (response.data.status) {
                            Toast.fire({
                                icon: "success",
                                title: `${response.data.message}`
                            });
                        } else {
                            Toast.fire({
                                icon: "error",
                                title: `${response.data.message}`
                            });
                        }
                    });
                }
            });
        });
    } else if ($("div").is(".promotional-inner")) {
        const purchaseForm = document.getElementById('purchaseForm');
        const id = parseInt(document.getElementById('packageName').getAttribute('data-id'));
        const increaseBtn = document.querySelector(".increase");
        const decreaseBtn = document.querySelector(".decrease");
        const quantityField = document.getElementById("quantity");
        let price = parseInt(
            document.getElementById("current_price").getAttribute("data-price")
        );
        let sum = document.getElementById("sum");
        let step = 1;
        decreaseBtn.addEventListener("click", e => {
            if (quantityField.value > 1) {
                --step;
                quantityField.value = quantityField.value - 1;
                sum.innerHTML = price * step + " тг";
                sum.setAttribute('data-sum', price * step);
            }
        });
        increaseBtn.addEventListener("click", e => {
            ++step;
            quantityField.value = parseInt(quantityField.value) + 1;
            sum.innerHTML = price * step + " тг";
            sum.setAttribute('data-sum', price * step);
        });
        purchaseForm.addEventListener('submit', function(e) {
            e.preventDefault();
            let user    = purchaseForm.getAttribute('data-user');
            if(user == 1) {
                console.log(e.target);
                let data = new FormData(this);
                data.append('package_id', id);
                data.append('sum', parseInt(sum.getAttribute('data-sum')));
                data.append('count', parseInt(quantityField.value));
                axios.post('/package', data).then(response => {
                    window.location.replace(`/buy/${response.data.order_id}`);
                })
            }else{
                Swal.fire({
                    icon: "warning",
                    text: 'Авторизуйтесь, пожалуйста!'
                });
            }
        })
    }
};

$(".promotional-slider").slick({
    infinite: true,
    centerMode: true,
    centerPadding: "20px",
    nextArrow: '<i class="next-btn fal fa-chevron-right"></i>',
    prevArrow: '<i class="prev-btn fal fa-chevron-left"></i>',
    slidesToShow: 3,
    slidesToScroll: 1
});

$(document).ready(function() {
    $(".magific-pop").magnificPopup({
        type: "image",
        closeOnContentClick: true,
        mainClass: "mfp-img-mobile",
        image: {
            verticalFit: true
        }
    });
});

const review_btn = document.querySelector(".leave-review");
review_btn.addEventListener("click", function(e) {
    e.preventDefault();
    Swal.fire({
        title: "Ваш отзыв",
        confirmButtonText: "Оставить отзыв",
        focusConfirm: false,
        html: `<form class="review-form">
                    <div class="form-group">
                        <input type="text" name="username" placeholder="Имя">
                    </div>
                    <div>
                        <input type="text" name="position" placeholder="Должность">
                    </div>
                  <div class="form-group">
                      <textarea name="message" placeholder="Отзыв"></textarea>
                  </div>
                  <div class="form-group">
                      <div class="star-rating">
                        <input type="radio" value="5" name="rating" id="rating-5">
                        <label class="star-rating__star" for="rating-5">
                            <i class="fad fa-star fa-lg"></i>
                        </label>
                        <input type="radio" value="4" name="rating" id="rating-4">
                        <label class="star-rating__star" for="rating-4">
                            <i class="fad fa-star fa-lg"></i>
                        </label>
                        <input type="radio" value="3" name="rating" id="rating-3">
                        <label class="star-rating__star" for="rating-3">
                            <i class="fad fa-star fa-lg"></i>
                        </label>
                        <input type="radio" value="2" name="rating" id="rating-2">
                        <label class="star-rating__star" for="rating-2">
                            <i class="fad fa-star fa-lg"></i>
                        </label>
                        <input type="radio" value="1" name="rating" id="rating-1">
                        <label class="star-rating__star" for="rating-1">
                            <i class="fad fa-star fa-lg"></i>
                        </label>
                      </div>
                  </div>
                </form>`,
        preConfirm: () => {
            let reviewForm = document.querySelector(".review-form");
            let review = new FormData(reviewForm);
            return axios
                .post("/reviews/add", review)
                .then(response => {
                    Swal.fire({
                        icon: "success",
                        text: response.data.message
                    });
                    document.querySelector(".review-form").reset();
                    console.log(response);
                })
                .catch(function(error) {
                    Swal.showValidationMessage(error.response.data.message);
                });
        }
    });
});
