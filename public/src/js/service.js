import axios from "axios";
import NProgress from "../../node_modules/nprogress/nprogress.js";

const apiClient = axios.create({
    baseURL: window.location.origin,
    withCredentials: false,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
    }
});

apiClient.interceptors.response.use(
    response => {
        return response;
    },
    error => {
        if (error.response.status === 401) {
            return;
        }
        return error;
    }
);

apiClient.interceptors.request.use(config => {
    NProgress.start();
    return config;
});

apiClient.interceptors.response.use(response => {
    NProgress.done();
    return response;
});

export default {
    getUserData() {
        return apiClient.get("/profile/index");
    },
    updateUserData(data) {
        return apiClient.post("/profile/update", data);
    },
    updateUserPassword(pwd) {
        return apiClient.post("/password/update", pwd);
    }
};
