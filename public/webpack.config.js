const path = require('path');


module.exports = {
    mode: "development",
    entry: {
        main: './src/js/main.js',
    },
    output: {
        filename: '[name].js',
        path: path.join(__dirname, './dist/js')
    },
    module: {
        rules: [
            {
                test: /\.(scss)$/,
                use: [
                {
                    loader: 'style-loader'
                },
                {
                    loader: 'css-loader'
                },
                {
                    loader: 'postcss-loader',
                    options: {
                    plugins: function () {
                        return [
                        require('autoprefixer')
                        ];
                    }
                    }
                },
                {
                    loader: 'sass-loader'
                }
                ]
            }
        ]
    }
};