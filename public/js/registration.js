$('#authorization-form').on('submit', function (e) {
    e.preventDefault();
    let form = new FormData(document.forms.authorizationForm);
    axios.post('', form)
        .then(function (response) {
            if(response.data.status === 1) {
                Swal.fire({
                    icon: 'success',
                    text: response.data.message
                });
            } else if(response.data.status === 0){
                Swal.fire({
                    icon: 'warning',
                    text: response.data.message
                });
            }

        })
        .catch(function (error) {
            // console.log(error.response)
            Swal.fire({
                icon: 'error',
                text: error.response.data.message
            });
        })
});