$("#question-lawyer").on("submit", function(e) {
    e.preventDefault();
    let form = new FormData(document.forms.questionLawyer);
    var selected_option = $("#dropdownMenuButton option:selected")[0];

    form.append("userServiceId", selected_option.getAttribute("data-id"));

    axios
        .post("/user/question", form)
        .then(function(response) {
            Swal.fire({
                icon: "success",
                html: response.data.message
            });
            document.getElementById("question-lawyer").reset();
            setTimeout(function() {
                window.location.href = "/profile";
            }, 2000);
        })
        .catch(function(error) {
            // console.log(error.response)
            Swal.fire({
                icon: "error",
                text: error.response.data.message
            });
        });
});
