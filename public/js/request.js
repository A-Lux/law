const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger',
    },
    buttonsStyling: false
});

$('#request-user').on('submit', function (e) {
    e.preventDefault();
    let form = new FormData(document.forms.requestUser);
    axios.post('/request/create', form)
        .then(function (response) {
            if(response.data.status === 1) {
                Swal.fire({
                    icon: 'success',
                    text: response.data.message
                });
                document.getElementById('request-user').reset();
            } else if(response.data.status === 0){
                Swal.fire({
                    icon: 'warning',
                    text: response.data.message
                });
            }

        })
        .catch(function (error) {
            // console.log(error.response)
            Swal.fire({
                icon: 'error',
                text: error.response.data.message
            });
        })
});

$('#support-service').on('submit', function (e) {
    e.preventDefault();
    let form = new FormData(document.forms.supportService);
    axios.post('/request/support', form)
        .then(function (response) {

            if(response.data.status === 1) {
                Swal.fire({
                    icon: 'success',
                    text: response.data.message
                });
                document.getElementById('support-service').reset();
            } else if(response.data.status === 0){
                Swal.fire({
                    icon: 'warning',
                    text: response.data.message
                });
            }

        })
        .catch(function (error) {
            Swal.fire({
                icon: 'error',
                text: error.response.data.message
            });
        })
});

$('#callback-user').on('submit', function (e) {
    e.preventDefault();
    let form = new FormData(document.forms.callbackUser);
    axios.post('/request/package', form)
        .then(function (response) {
            if(response.data.status === 1) {
                Swal.fire({
                    icon: 'success',
                    text: response.data.message
                });
                document.getElementById('callback-user').reset();
            } else if(response.data.status === 0){
                Swal.fire({
                    icon: 'warning',
                    text: response.data.message
                });
            }

        })
        .catch(function (error) {
            // console.log(error.response)
            Swal.fire({
                icon: 'error',
                text: error.response.data.message
            });
        })
});