<?php
require_once("header.php");
?>
<div class="docs">
    <div class="slider-wrapper">
        <div class="container over py-5">
            <div class="row py-5">
                <div class="col-lg-1 d-flex align-items-center justify-content-center"><i class="prev-btn fal fa-chevron-left"></i></div>
                <div class="offset-lg-7 offset-md-6"></div>
                <div class="col-lg-3 col-md-6">
                    <div class="request-block">
                        <img src="./images/comment-alt-lines.png" class="comment">
                        <h1 class="title">Вы можете <span>прямо сейчас</span> проконсультироваться</h1>
                        <p class="description">Оставьте свой номер телефона и мы вам перезвоним</p>
                        <form action="" class="send-request">
                            <div class="form-group">
                                <input type="text" name="name" placeholder="Ваше имя" class="name">
                            </div>
                            <div class="form-group">
                                <input type="text" name="phone" placeholder="Введите ваш телефон" class="phone">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="send">Оставить заявку</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-1 d-flex align-items-center justify-content-center"><i class="next-btn fal fa-chevron-right"></i></div>
            </div>
        </div>
        <div class="main-slider slider">
            <div>
                <div class="item" style="background-image: url('./images/law-banner.png')">
                    <div class="container py-5">
                        <div class="row py-5">
                            <div class="offset-lg-1"></i></div>
                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                <h1 class="title">Решаем юридические споры вне зависимости от сложности</h1>
                                <p class="description">Поможем вам в любых ситуациях: взыскание долгов,абонентское обслуживание бизнеса, разработка правовых документов, регистрация ТОО</p>
                                <a href="#" class="check-details">Посмотреть подробнее</a>
                            </div>
                            <div class="offset-lg-5 offset-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="item" style="background-image: url('./images/law-banner.png')">
                    <div class="container py-5">
                        <div class="row py-5">
                            <div class="offset-lg-1"></i></div>
                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                <h1 class="title">Решаем юридические споры вне зависимости от сложности</h1>
                                <p class="description">Поможем вам в любых ситуациях: взыскание долгов,абонентское обслуживание бизнеса, разработка правовых документов, регистрация ТОО</p>
                                <a href="#" class="check-details">Посмотреть подробнее</a>
                            </div>
                            <div class="offset-lg-5 offset-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="item" style="background-image: url('./images/law-banner.png')">
                    <div class="container py-5">
                        <div class="row py-5">
                            <div class="offset-lg-1"></i></div>
                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                <h1 class="title">Решаем юридические споры вне зависимости от сложности</h1>
                                <p class="description">Поможем вам в любых ситуациях: взыскание долгов,абонентское обслуживание бизнеса, разработка правовых документов, регистрация ТОО</p>
                                <a href="#" class="check-details">Посмотреть подробнее</a>
                            </div>
                            <div class="offset-lg-5 offset-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="item" style="background-image: url('./images/law-banner.png')">
                    <div class="container py-5">
                        <div class="row py-5">
                            <div class="offset-lg-1"></i></div>
                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                <h1 class="title">Решаем юридические споры вне зависимости от сложности</h1>
                                <p class="description">Поможем вам в любых ситуациях: взыскание долгов,абонентское обслуживание бизнеса, разработка правовых документов, регистрация ТОО</p>
                                <a href="#" class="check-details">Посмотреть подробнее</a>
                            </div>
                            <div class="offset-lg-5 offset-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#civil-law" class="anchor">
            <i class="fal fa-anchor"></i>
        </a>
    </div>
    <div class="civil-law" id="civil-law">
        <div class="container my-sm-5">
            <div class="row mb-3">
                <div class="col-lg-12">
                    <h1 class="title">Гражданское право.<br>Договора гражданско-правового характера.</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="description">
                        <ol>
                            <li data-count="1">
                                <p>Гражданско-правовые договора, предусмотренные специальными главами Гражданского кодекса РК (купля-продажа, поставка, аренда, заем, дарение, подряд, перевозка, лизинг, прокат, хранение, поручение, комиссия, франчайзинг, контрактация, мена, рента).</p>
                            </li>
                            <li data-count="2">
                                <p>Гражданско-правовые договора, указанные в Гражданском кодексе, но не предусмотренные специальными главами ГК РК.</p>
                            </li>
                            <li data-count="3">
                                <p>Гражданско-правовые договора (контракты), не указанные в ГК, но не противоречащие ему (включая сложные, смешанные и т.п.).</p>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="labor-law">
        <h1 class="title">Трудовое право. Кадровое делопроизводство</h1>
        <div class="container my-5">
            <div class="row">
                <div class="col-lg-12">
                    <div class="description">
                        <ol>
                            <li>
                                <p>Трудовой договор.</p>
                            </li>
                            <li>
                                <p>Договор о материальной ответственности.</p>
                            </li>
                            <li>
                                <p>Соглашение (о неразглашении конфиденциальной информации, о неконкуренции, обработки персональных данных).</p>
                            </li>
                            <li>
                                <p>Положения (о защите персональных данных работников, о коммерческой тайне, об оплате труда, о нормировании труда и т.д.).</p>
                            </li>
                            <li>
                                <p>Должностные инструкции.</p>
                            </li>
                            <li>
                                <p>Правила внутреннего трудового распорядка.</p>
                            </li>
                            <li>
                                <p>Коллективный договор.</p>
                            </li>
                            <li>
                                <p>Номенклатура дел.</p>
                            </li>
                            <li>
                                <p>Штатное расписание.</p>
                            </li>
                            <li>
                                <p>Приказы (о приеме на работу, увольнении, отпусках, взысканиях и т. д.).</p>
                            </li>
                            <li>
                                <p>Подготовка документов и актов работодателя по охране труда и техники безопасности (инструкции, положения) по вопросам организации и обеспечения охраны труда и пр.</p>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="constituent-documents">
        <div class="title">
            Учредительные документы для ТОО
        </div>
        <div class="container my-5">
            <div class="row">
                <div class="col-lg-12">
                    <div class="description">
                        <ol>
                            <li data-count="1">
                                <p>Устав с одним или с несколькими участниками.</p>
                            </li>
                            <li data-count="2">
                                <p>Учредительный договор.</p>
                            </li>
                            <li data-count="3">
                                <p>Решение единственного участника о создании ТОО.</p>
                            </li>
                            <li data-count="4">
                                <p>Протокол общего собрания о создании ТОО.</p>
                            </li>
                            <li data-count="5">
                                <p>Приказ о назначении руководителя.</p>
                            </li>
                            <li data-count="6">
                                <p>Решение единственного участника товарищества об уменьшении уставного капитала товарищества.</p>
                            </li>
                            <li data-count="7">
                                <p>Решение единственного участника об утверждении устава в новой редакции.</p>
                            </li>
                            <li data-count="8">
                                <p>Решение единственного участника о назначении (избрании) и (или) досрочном. прекращении полномочий руководителя исполнительного органа товарищества. </p>
                            </li>
                            <li data-count="9">
                                <p>Протокол общего собрания участников об утверждении устава в новой редакции.</p>
                            </li>
                            <li data-count="10">
                                <p>Пртокол общего собрания участников о назначении (избрании) и (или) досрочном. прекращении полномочий руководителя исполнительного органа товарищества.</p>
                            </li>
                            <li data-count="11">
                                <p></p>
                            </li>
                            <li data-count="12">
                                <p>Протокол общего собрания участников о продаже или приобретении основных средств.</p>
                            </li>
                            <li data-count="13">
                                <p>Протокол общего собрания участников об уменьшении уставного капитала товарищества.</p>
                            </li>
                            <li data-count="14">
                                <p>Протокол общего собрания участников о перерегистрации товарищества в связи с изменением наименования.</p>
                            </li>
                            <li data-count="15">
                                <p>Протокол общего собрания об отчуждении доли.</p>
                            </li>
                            <li data-count="16">
                                <p>Протокол общего собрания о создании наблюдательного совета.</p>
                            </li>
                            <li data-count="17">
                                <p>Протокол общего собрания в связи с изменением состава участников.</p>
                            </li>
                            <li data-count="18">
                                <p>Изменения в Устав в связи с изменениями в составе участников.</p>
                            </li>
                            <li data-count="19">
                                <p>Соглашение о внесении изменений в Учредительный договор (при смене участника Товарищества).</p>
                            </li>
                            <li data-count="20">
                                <p>Положение о наблюдательном совете.</p>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="get-service">
        <h1 class="title">
            Как получить услугу:
        </h1>
        <div class="container my-5">
            <div class="row">
                <div class="col-lg-12">
                    <div class="description">
                        <ol>
                            <li>
                                <p>Зарегистрируйтесь на сайте.</p>
                            </li>
                            <li>
                                <p>Вам необходимо приобрести пакет Halyk стоимостью 10 000 тенге и в рамках пакета заказать документ. Если документ относится к категории «сложный или смешанный» или вам нужно разработать несколько документов, то необходимо будет приобрести от 2 и более пакетов Halyk. </p>
                            </li>
                            <li>
                                <p>Приобретая пакет Halyk, Вы становитесь участником розыгрыша ценных подарков, а также получите скидку в размере 10% на </p>
                            </li>
                        </ol>
                        <a href="#" class="buy">купить</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once("footer.php");
?>