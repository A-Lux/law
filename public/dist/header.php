<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no" />
    <link rel="stylesheet" href="./css/main.css">
    <title>Буква закона</title>
</head>
<body>
    <div class="header">
        <div class="container">
            <div class="row py-4">
                <div class="col-lg-2 col-md-4 col-6 mb-4 mb-lg-0">
                    <a href="/" class="logo">
                        <img src="./images/logo.png">
                    </a>
                </div>
                <div class="col-lg-5 col-md-8 col-6 mb-4 mb-lg-0">
                    <a href="" class="toggle">
                        <span></span>
                    </a>
                    <nav class="main-nav">
                        <ul>
                            <li><a href="/about.php">О компании</a></li>
                            <li><a href="/advantages.php">Наши преимущества</a></li>
                            <li><a href="/docs.php">Наш опыт</a></li>
                            <li><a href="/promotional.php">Рекомендации</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-3 col-6 d-flex flex-lg-row flex-wrap justify-content-between align-items-lg-center">
                    <a href="#" class="registration-btn">Зарегистрироваться</a>
                    <a href="#" class="authorization-btn">Войти</a>
                </div>
                <div class="col-lg-2 col-6 d-md-flex flex-md-column flex-md-wrap justify-content-md-center align-items-md-end">
                    <a href="tel: +7 (727) 317-16-98" class="phone-number">+7 (727) 317-16-98</a>
                    <a href="#" class="callback-btn">Заказать обратный звонок</a>
                </div>
            </div>
        </div>
    </div>