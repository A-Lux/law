<?php
require_once("header.php");
?>
<div class="promotional">
    <div class="slider-wrapper">
        <div class="container over py-5">
            <div class="row py-5">
                <div class="col-lg-1 d-flex align-items-center justify-content-center"><i class="prev-btn fal fa-chevron-left"></i></div>
                <div class="offset-lg-7 offset-md-6"></div>
                <div class="col-lg-3 col-md-6">
                    <div class="request-block">
                        <img src="./images/comment-alt-lines.png" class="comment">
                        <h1 class="title">Вы можете <span>прямо сейчас</span> проконсультироваться</h1>
                        <p class="description">Оставьте свой номер телефона и мы вам перезвоним</p>
                        <form action="" class="send-request">
                            <div class="form-group">
                                <input type="text" name="name" placeholder="Ваше имя" class="name">
                            </div>
                            <div class="form-group">
                                <input type="text" name="phone" placeholder="Введите ваш телефон" class="phone">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="send">Оставить заявку</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-1 d-flex align-items-center justify-content-center"><i class="next-btn fal fa-chevron-right"></i></div>
            </div>
        </div>
        <div class="main-slider slider">
            <div>
                <div class="item" style="background-image: url('./images/law-banner.png')">
                    <div class="container py-5">
                        <div class="row py-5">
                            <div class="offset-lg-1"></i></div>
                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                <h1 class="title">Решаем юридические споры вне зависимости от сложности</h1>
                                <p class="description">Поможем вам в любых ситуациях: взыскание долгов,абонентское обслуживание бизнеса, разработка правовых документов, регистрация ТОО</p>
                                <a href="#" class="check-details">Посмотреть подробнее</a>
                            </div>
                            <div class="offset-lg-5 offset-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="item" style="background-image: url('./images/law-banner.png')">
                    <div class="container py-5">
                        <div class="row py-5">
                            <div class="offset-lg-1"></i></div>
                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                <h1 class="title">Решаем юридические споры вне зависимости от сложности</h1>
                                <p class="description">Поможем вам в любых ситуациях: взыскание долгов,абонентское обслуживание бизнеса, разработка правовых документов, регистрация ТОО</p>
                                <a href="#" class="check-details">Посмотреть подробнее</a>
                            </div>
                            <div class="offset-lg-5 offset-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="item" style="background-image: url('./images/law-banner.png')">
                    <div class="container py-5">
                        <div class="row py-5">
                            <div class="offset-lg-1"></i></div>
                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                <h1 class="title">Решаем юридические споры вне зависимости от сложности</h1>
                                <p class="description">Поможем вам в любых ситуациях: взыскание долгов,абонентское обслуживание бизнеса, разработка правовых документов, регистрация ТОО</p>
                                <a href="#" class="check-details">Посмотреть подробнее</a>
                            </div>
                            <div class="offset-lg-5 offset-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="item" style="background-image: url('./images/law-banner.png')">
                    <div class="container py-5">
                        <div class="row py-5">
                            <div class="offset-lg-1"></i></div>
                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                <h1 class="title">Решаем юридические споры вне зависимости от сложности</h1>
                                <p class="description">Поможем вам в любых ситуациях: взыскание долгов,абонентское обслуживание бизнеса, разработка правовых документов, регистрация ТОО</p>
                                <a href="#" class="check-details">Посмотреть подробнее</a>
                            </div>
                            <div class="offset-lg-5 offset-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#codes" class="anchor">
            <i class="fal fa-anchor"></i>
        </a>
    </div>
    <div class="codes" id="codes">
        <div class="container">
            <div class="row">
                <div class="offset-lg-4 offset-md-5"></div>
                <div class="col-lg-8 col-md-7">
                    <h1 class="title">Промокоды</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-5 col-12">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link" id="v-pills-personal-tab" data-toggle="pill" href="#v-pills-personal">Персональные данные</a>
                        <a class="nav-link" id="v-pills-password-tab" data-toggle="pill" href="#v-pills-password">Смена пароля</a>
                        <a class="nav-link" id="v-pills-halyk-tab" data-toggle="pill" href="#v-pills-halyk" role="tab" aria-controls="v-pills-halyk" aria-selected="false">Пакет Halyk</a>
                        <a class="nav-link" id="v-pills-business-tab" data-toggle="pill" href="#v-pills-business" role="tab" aria-controls="v-pills-business" aria-selected="false">Пакет Business (файл Business пакет);</a>
                        <a class="nav-link" id="v-pills-gifts-tab" data-toggle="pill" href="#v-pills-gifts" role="tab" aria-controls="v-pills-gifts" aria-selected="false">Скачать подарки</a>
                        <a class="nav-link" id="v-pills-question-tab" data-toggle="pill" href="#v-pills-question" role="tab" aria-controls="v-pills-question" aria-selected="false">Задать вопрос юристу</a>
                        <a class="nav-link active" id="v-pills-codes-tab" data-toggle="pill" href="#v-pills-codes" role="tab" aria-controls="v-pills-codes" aria-selected="false">Промокоды</a>
                        <a class="nav-link" id="v-pills-consultations-tab" data-toggle="pill" href="#v-pills-consultations" role="tab" aria-controls="v-pills-consultations" aria-selected="false">Автоматическое списывание консультаций</a>
                    </div>
                </div>
                <div class="col-lg-8 col-md-7 col-12">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade" id="v-pills-halyk" role="tabpanel" aria-labelledby="v-pills-halyk-tab">...</div>
                        <div class="tab-pane fade" id="v-pills-business" role="tabpanel" aria-labelledby="v-pills-business-tab">...</div>
                        <div class="tab-pane fade" id="v-pills-gifts" role="tabpanel" aria-labelledby="v-pills-gifts-tab">...</div>
                        <div class="tab-pane fade" id="v-pills-question" role="tabpanel" aria-labelledby="v-pills-question-tab">...</div>
                        <div class="tab-pane fade show active" id="v-pills-codes" role="tabpanel" aria-labelledby="v-pills-codes-tab">
                            <div class="promotional-codes">
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                                <p class="code">12345</p>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-consultations" role="tabpanel" aria-labelledby="v-pills-consultations-tab">...</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once("footer.php");
?>