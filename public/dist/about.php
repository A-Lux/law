<?php
require_once("header.php")
?>
<div class="about">
    <div class="slider-wrapper">
        <div class="container over py-5">
            <div class="row py-5">
                <div class="col-lg-1 d-flex align-items-center justify-content-center"><i class="prev-btn fal fa-chevron-left"></i></div>
                <div class="offset-lg-7 offset-md-6"></div>
                <div class="col-lg-3 col-md-6">
                    <div class="request-block">
                        <img src="./images/comment-alt-lines.png" class="comment">
                        <h1 class="title">Вы можете <span>прямо сейчас</span> проконсультироваться</h1>
                        <p class="description">Оставьте свой номер телефона и мы вам перезвоним</p>
                        <form action="" class="send-request">
                            <div class="form-group">
                                <input type="text" name="name" placeholder="Ваше имя" class="name">
                            </div>
                            <div class="form-group">
                                <input type="text" name="phone" placeholder="Введите ваш телефон" class="phone">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="send">Оставить заявку</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-1 d-flex align-items-center justify-content-center"><i class="next-btn fal fa-chevron-right"></i></div>
            </div>
        </div>
        <div class="main-slider slider">
            <div>
                <div class="item" style="background-image: url('./images/law-banner.png')">
                    <div class="container py-5">
                        <div class="row py-5">
                            <div class="offset-lg-1"></i></div>
                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                <h1 class="title">Решаем юридические споры вне зависимости от сложности</h1>
                                <p class="description">Поможем вам в любых ситуациях: взыскание долгов,абонентское обслуживание бизнеса, разработка правовых документов, регистрация ТОО</p>
                                <a href="#" class="check-details">Посмотреть подробнее</a>
                            </div>
                            <div class="offset-lg-5 offset-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="item" style="background-image: url('./images/law-banner.png')">
                    <div class="container py-5">
                        <div class="row py-5">
                            <div class="offset-lg-1"></i></div>
                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                <h1 class="title">Решаем юридические споры вне зависимости от сложности</h1>
                                <p class="description">Поможем вам в любых ситуациях: взыскание долгов,абонентское обслуживание бизнеса, разработка правовых документов, регистрация ТОО</p>
                                <a href="#" class="check-details">Посмотреть подробнее</a>
                            </div>
                            <div class="offset-lg-5 offset-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="item" style="background-image: url('./images/law-banner.png')">
                    <div class="container py-5">
                        <div class="row py-5">
                            <div class="offset-lg-1"></i></div>
                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                <h1 class="title">Решаем юридические споры вне зависимости от сложности</h1>
                                <p class="description">Поможем вам в любых ситуациях: взыскание долгов,абонентское обслуживание бизнеса, разработка правовых документов, регистрация ТОО</p>
                                <a href="#" class="check-details">Посмотреть подробнее</a>
                            </div>
                            <div class="offset-lg-5 offset-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="item" style="background-image: url('./images/law-banner.png')">
                    <div class="container py-5">
                        <div class="row py-5">
                            <div class="offset-lg-1"></i></div>
                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                <h1 class="title">Решаем юридические споры вне зависимости от сложности</h1>
                                <p class="description">Поможем вам в любых ситуациях: взыскание долгов,абонентское обслуживание бизнеса, разработка правовых документов, регистрация ТОО</p>
                                <a href="#" class="check-details">Посмотреть подробнее</a>
                            </div>
                            <div class="offset-lg-5 offset-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#about-us" class="anchor">
            <i class="fal fa-anchor"></i>
        </a>
    </div>
    <div class="about-us" id="about-us">
        <div class="container">
            <div class="row my-5">
                <div class="col-lg-12">
                    <h1 class="title">
                        О компании
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="description">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam semper sed turpis sed sollicitudin. Aliquam vitae aliquam eros. Nunc nec posuere diam, ut congue arcu. Vivamus egestas ipsum at metus varius dictum. Pellentesque vehicula blandit lacus ac aliquet. Aenean luctus nulla nec fermentum consequat. Praesent elementum urna lectus, vitae vestibulum odio imperdiet eget. Praesent et lectus dapibus, placerat ante vitae, porttitor diam. Quisque ullamcorper lacinia efficitur. Donec vel quam tincidunt, imperdiet ante fringilla, tincidunt elit.</p>
                        <p>Vivamus bibendum venenatis ornare. Vivamus pretium ligula leo, at sagittis diam iaculis sit amet. Phasellus sit amet blandit sem. Ut non nulla efficitur, luctus velit a, molestie dui. Donec iaculis tristique ipsum, sed laoreet massa rhoncus eget. Phasellus vestibulum sit amet nulla sit amet tristique. Curabitur semper imperdiet mollis.</p>
                        <p>Vivamus sapien sapien, ullamcorper lobortis metus vel, fermentum accumsan nisi. Integer egestas mi tincidunt diam congue, quis egestas urna cursus. Curabitur nunc velit, pharetra varius ligula eget, blandit efficitur arcu. Nam volutpat ante sed semper varius. In lacinia pretium dui, tincidunt efficitur magna gravida et. Sed maximus lacus dictum, malesuada lorem et, imperdiet erat. Etiam felis ante, rhoncus eu risus non, accumsan sagittis eros. Phasellus sed malesuada leo. Donec pretium blandit scelerisque. Donec eros arcu, consectetur in nibh nec, elementum hendrerit lorem. In sed leo non</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="video my-5">
        <div class="container">
            <div class="row py-5">
                <div class="col-lg-12 d-flex justify-content-center">
                    <figure class="video-wrapper">
                        <img src="./images/tv.png">
                        <div class="fluid-width-video-wrapper">
                            <video loop="" playsinline="" poster="./images/poster.png">
                                <source src="./videos/mov_bbb.mp4" type="video/mp4" codecs="avc1.42E01E, mp4a.40.2">
                            </video>
                            <button type="button" id="play-btn">
                                <div class="first-circle"></div>
                                <div class="second-circle"></div>
                            </button>
                        </div>
                        <figcaption>Смотрите <span>видео</span> о нашей компании</figcaption>
                    </figure>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once("footer.php")
?>