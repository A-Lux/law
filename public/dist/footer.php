    <div class="footer">
        <div class="container">
            <div class="row py-4">
                <div class="col-lg-2 col-md-3 col-sm-6 col-12 order-sm-0">
                    <a href="#" class="logo">
                        <img src="./images/footers-logo.png">
                    </a>
                </div>
                <div class="col-lg-2 col-md-3 col-12 d-flex justify-content-sm-end justify-content-md-start align-items-lg-center order-sm-1">
                    <ul class="map">
                        <li><a href="#">Правила розыгрыша</a></li>
                        <li><a href="">Публичная оферта</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-3 col-12 d-flex align-items-lg-center justify-content-md-start order-sm-2">
                    <ul class="map">
                        <li><a href="#">Служба поддержки</a></li>
                        <li><a href="#">Отзывы и предложения</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-6 col-12 d-flex flex-column flex-wrap justify-content-sm-center align-items-sm-end order-sm-0 order-md-last order-lg-3 order-last">
                    <a href="tel: +7 (727) 317-16-98" class="phone-number">+7 (727) 317-16-98</a>
                    <a href="mailto: mail@info.com" class="e-mail">mail@info.com</a>
                </div>
                <div class="col-lg-2 col-md-3 col-12 d-flex justify-content-sm-end justify-content-md-start align-items-lg-center order-sm-4">
                    <p class="address">г. Алматы, <span>ул. Островского 17</span></p>
                </div>
                <div class="offset-lg-1">

                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-12 order-last order-md-0">
                    <p class="copyright">
                        Все права защищены<br>
                        (с) Юридическая компания „BUKVA ZAKONA“, 2020
                    </p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-8 col-6 d-lg-flex align-items-lg-center">
                    <p class="socials-title">
                        Подписывайтесь на нас в соцсетях:
                    </p>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                    <ul class="socials-map">
                        <li>
                            <a href="#">
                                <i class="fab fa-facebook-square"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-telegram-plane"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                    <p class="developed-by">Разработано в <a href="#"><img src="./images/a-lux.png" alt=""></a></p>
                </div>
                <div class="offset-lg-1"></div>
            </div>
        </div>
    </div>
    <script src="./js/main.js"></script>
    </body>

    </html>