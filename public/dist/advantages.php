<?php
require_once('header.php');
?>
<div class="advantages">
    <div class="slider-wrapper">
        <div class="container over py-5">
            <div class="row py-5">
                <div class="col-lg-1 d-flex align-items-center justify-content-center"><i class="prev-btn fal fa-chevron-left"></i></div>
                <div class="offset-lg-7 offset-md-6"></div>
                <div class="col-lg-3 col-md-6">
                    <div class="request-block">
                        <img src="./images/comment-alt-lines.png" class="comment">
                        <h1 class="title">Вы можете <span>прямо сейчас</span> проконсультироваться</h1>
                        <p class="description">Оставьте свой номер телефона и мы вам перезвоним</p>
                        <form action="" class="send-request">
                            <div class="form-group">
                                <input type="text" name="name" placeholder="Ваше имя" class="name">
                            </div>
                            <div class="form-group">
                                <input type="text" name="phone" placeholder="Введите ваш телефон" class="phone">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="send">Оставить заявку</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-1 d-flex align-items-center justify-content-center"><i class="next-btn fal fa-chevron-right"></i></div>
            </div>
        </div>
        <div class="main-slider slider">
            <div>
                <div class="item" style="background-image: url('./images/law-banner.png')">
                    <div class="container py-5">
                        <div class="row py-5">
                            <div class="offset-lg-1"></i></div>
                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                <h1 class="title">Решаем юридические споры вне зависимости от сложности</h1>
                                <p class="description">Поможем вам в любых ситуациях: взыскание долгов,абонентское обслуживание бизнеса, разработка правовых документов, регистрация ТОО</p>
                                <a href="#" class="check-details">Посмотреть подробнее</a>
                            </div>
                            <div class="offset-lg-5 offset-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="item" style="background-image: url('./images/law-banner.png')">
                    <div class="container py-5">
                        <div class="row py-5">
                            <div class="offset-lg-1"></i></div>
                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                <h1 class="title">Решаем юридические споры вне зависимости от сложности</h1>
                                <p class="description">Поможем вам в любых ситуациях: взыскание долгов,абонентское обслуживание бизнеса, разработка правовых документов, регистрация ТОО</p>
                                <a href="#" class="check-details">Посмотреть подробнее</a>
                            </div>
                            <div class="offset-lg-5 offset-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="item" style="background-image: url('./images/law-banner.png')">
                    <div class="container py-5">
                        <div class="row py-5">
                            <div class="offset-lg-1"></i></div>
                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                <h1 class="title">Решаем юридические споры вне зависимости от сложности</h1>
                                <p class="description">Поможем вам в любых ситуациях: взыскание долгов,абонентское обслуживание бизнеса, разработка правовых документов, регистрация ТОО</p>
                                <a href="#" class="check-details">Посмотреть подробнее</a>
                            </div>
                            <div class="offset-lg-5 offset-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="item" style="background-image: url('./images/law-banner.png')">
                    <div class="container py-5">
                        <div class="row py-5">
                            <div class="offset-lg-1"></i></div>
                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                <h1 class="title">Решаем юридические споры вне зависимости от сложности</h1>
                                <p class="description">Поможем вам в любых ситуациях: взыскание долгов,абонентское обслуживание бизнеса, разработка правовых документов, регистрация ТОО</p>
                                <a href="#" class="check-details">Посмотреть подробнее</a>
                            </div>
                            <div class="offset-lg-5 offset-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#our-advantages" class="anchor">
            <i class="fal fa-anchor"></i>
        </a>
    </div>
    <div class="our-advantages" id="our-advantages">
        <div class="container">
            <div class="row my-5">
                <div class="col-lg-12">
                    <h1 class="title">Наши приемущества</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="description">
                        <ul>
                            <li data-aos="fade-right"><img src="./images/advantage.png"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac quam odio. Sed luctus sit. </li>
                            <li data-aos="fade-right"><img src="./images/advantage.png"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit.</li>
                            <li data-aos="fade-right"><img src="./images/advantage.png"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac quam odio</li>
                            <li data-aos="fade-right"><img src="./images/advantage.png"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </li>
                            <li data-aos="fade-right"><img src="./images/advantage.png"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac quam odio. Sed luctus sit.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once('footer.php');
?>