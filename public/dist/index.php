<?php
require_once("header.php");
?>
<div class="main">
    <div class="slider-wrapper">
        <div class="container over py-5">
            <div class="row py-5">
                <div class="col-lg-1 d-flex align-items-center justify-content-center"><i class="prev-btn fal fa-chevron-left"></i></div>
                <div class="offset-lg-7 offset-md-6"></div>
                <div class="col-lg-3 col-md-6">
                    <div class="request-block">
                        <img src="./images/comment-alt-lines.png" class="comment">
                        <h1 class="title">Вы можете <span>прямо сейчас</span> проконсультироваться</h1>
                        <p class="description">Оставьте свой номер телефона и мы вам перезвоним</p>
                        <form action="" class="send-request">
                            <div class="form-group">
                                <input type="text" name="name" placeholder="Ваше имя" class="name">
                            </div>
                            <div class="form-group">
                                <input type="text" name="phone" placeholder="Введите ваш телефон" class="phone">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="send">Оставить заявку</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-1 d-flex align-items-center justify-content-center"><i class="next-btn fal fa-chevron-right"></i></div>
            </div>
        </div>
        <div class="main-slider slider">
            <div>
                <div class="item" style="background-image: url('./images/law-banner.png')">
                    <div class="container py-5">
                        <div class="row py-5">
                            <div class="offset-lg-1"></i></div>
                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                <h1 class="title">Решаем юридические споры вне зависимости от сложности</h1>
                                <p class="description">Поможем вам в любых ситуациях: взыскание долгов,абонентское обслуживание бизнеса, разработка правовых документов, регистрация ТОО</p>
                                <a href="#" class="check-details">Посмотреть подробнее</a>
                            </div>
                            <div class="offset-lg-5 offset-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="item" style="background-image: url('./images/law-banner.png')">
                    <div class="container py-5">
                        <div class="row py-5">
                            <div class="offset-lg-1"></i></div>
                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                <h1 class="title">Решаем юридические споры вне зависимости от сложности</h1>
                                <p class="description">Поможем вам в любых ситуациях: взыскание долгов,абонентское обслуживание бизнеса, разработка правовых документов, регистрация ТОО</p>
                                <a href="#" class="check-details">Посмотреть подробнее</a>
                            </div>
                            <div class="offset-lg-5 offset-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="item" style="background-image: url('./images/law-banner.png')">
                    <div class="container py-5">
                        <div class="row py-5">
                            <div class="offset-lg-1"></i></div>
                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                <h1 class="title">Решаем юридические споры вне зависимости от сложности</h1>
                                <p class="description">Поможем вам в любых ситуациях: взыскание долгов,абонентское обслуживание бизнеса, разработка правовых документов, регистрация ТОО</p>
                                <a href="#" class="check-details">Посмотреть подробнее</a>
                            </div>
                            <div class="offset-lg-5 offset-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="item" style="background-image: url('./images/law-banner.png')">
                    <div class="container py-5">
                        <div class="row py-5">
                            <div class="offset-lg-1"></i></div>
                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                                <h1 class="title">Решаем юридические споры вне зависимости от сложности</h1>
                                <p class="description">Поможем вам в любых ситуациях: взыскание долгов,абонентское обслуживание бизнеса, разработка правовых документов, регистрация ТОО</p>
                                <a href="#" class="check-details">Посмотреть подробнее</a>
                            </div>
                            <div class="offset-lg-5 offset-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="services">
        <div class="container py-md-0 py-5">
            <div class="row my-5">
                <div class="col-lg-12">
                    <h1 class="title">Наши услуги</h1>
                </div>
                <div class="col-lg-4 col-sm-6 col-12 my-4" data-aos="fade-up">
                    <div class="service">
                        <figure>
                            <img src="./images/wallet.png">
                        </figure>
                        <p class="description">Взыскание долгов</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-12 my-4" data-aos="fade-up">
                    <div class="service">
                        <figure>
                            <img src="./images/passport.png">
                        </figure>
                        <p class="description">Пакет стандарт</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-12 my-4" data-aos="fade-up">
                    <div class="service">
                        <figure>
                            <img src="./images/report.png">
                        </figure>
                        <p class="description">Пакет Premium</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-12 my-4" data-aos="fade-up">
                    <div class="service">
                        <figure>
                            <img src="./images/user.png">
                        </figure>
                        <p class="description">Пакет Halyk для физических лиц</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-12 my-4" data-aos="fade-up">
                    <div class="service">
                        <figure>
                            <img src="./images/bag.png">
                        </figure>
                        <p class="description">Пакет Business</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-12 my-4" data-aos="fade-up">
                    <div class="service">
                        <figure>
                            <img src="./images/document.png">
                        </figure>
                        <p class="description">Партнерская программа</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="advantages">
        <div class="container">
            <div class="row py-5 my-5">
                <div class="col-lg-6 col-md-7 col-12">
                    <h1 class="title">BUKVA ZAKONA Юридический онлайн сервис</h1>
                    <div class="description">
                        <p><b>Онлайн сервис "BukvaZakona" - это:</b></p>
                        <p>Современный подход дистанционного юридического обслуживания</p>
                        <p>Оперативная и доступная юридическая поддержка для населения и бизнеса</p>
                        <p>Экономия времени - удобно, быстро и эффективно, так как клиент имеет возможность получить квалифицированную правовую помощь, не выходя из дома, на работе или в путешествии</p>
                    </div>
                    <div class="reward-cards">
                        <div class="card-body">
                            <h1 class="title">
                                более
                                <span>15</span>
                            </h1>
                            <p class="description">
                                Выигранных или частично выигранных дел
                            </p>
                        </div>
                        <div class="card-body">
                            <h1 class="title">
                                более
                                <span>100</span>
                            </h1>
                            <p class="description">
                                Постоянных клиентов
                            </p>
                        </div>
                    </div>
                </div>
                <div class="offset-lg-3"></div>
                <div class="col-lg-3 col-md-5 col-12 pt-4 pt-lg-0">
                    <div class="brochures-block">
                        <h1 class="title">Здесь вы можете бесплатно скачать статьи</h1>
                        <p class="description">на различные юридические темы с формами образцов документов</p>
                        <div class="brochure">
                            <a href="#">
                                <img src="./images/brochure1.png">
                            </a>
                            <h2 class="title">Взыскание долгов</h2>
                        </div>
                        <div class="brochure">
                            <a href="#">
                                <img src="./images/brochure2.png">
                            </a>
                            <h2 class="title">Составление правовых документов (договора, контракты, соглашения и т.д.)</h2>
                        </div>
                        <div class="brochure">
                            <a href="#">
                                <img src="./images/brochure3.png">
                            </a>
                            <h2 class="title">Договора займа между физическими лицами</h2>
                        </div>
                        <a href="#" class="more">Смотреть все статьи</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="reviews">
        <div class="container">
            <div class="row py-5">
                <div class="col-lg-12">
                    <h1 class="title">Отзывы</h1>
                </div>
            </div>
        </div>
        <div class="reviews-slider slider">
            <div>
                <div class="item">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-12 d-flex flex-column flex-md-column flex-sm-row align-items-md-start align-items-center justify-content-between">
                                <a class="avatar" href="#" style="background-image: url('../images/avatar1.png')">

                                </a>
                                <h2 class="initials">Beatriz Elorza, <b>architetta</b></h2>
                                <p class="date">17 мая</p>
                            </div>
                            <div class="col-lg-9 p-0 col-12 d-flex flex-column justify-content-between align-items-start">
                                <div class="message__wrapper">
                                    <div class="star-rating__wrapper" data-star="5">
                                        <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating">
                                        </label>
                                        <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating">
                                        </label>
                                        <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating">
                                        </label>
                                        <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating">
                                        </label>
                                        <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating">
                                        </label>
                                    </div>
                                    <p class="message">
                                        Повседневная практика показывает, что сложившаяся структура организации требуют определения и уточнения модели развития.
                                        <br>
                                        Задача организации, в особенности же укрепление и развитие структуры позволяет оценить значение соответствующий условий активизации. Не следует, однако забывать, что начало повседневной работы по формированию позиции позволяет выполнять важные задания по разработке дальнейших направлений развития.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="item">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-12 d-flex flex-column flex-md-column flex-sm-row align-items-md-start align-items-center justify-content-between">
                                <a class="avatar" href="#" style="background-image: url('../images/avatar1.png')">

                                </a>
                                <h2 class="initials">Beatriz Elorza, <b>architetta</b></h2>
                                <p class="date">17 мая</p>
                            </div>
                            <div class="col-lg-9 p-0 col-12 d-flex flex-column justify-content-between align-items-start">
                                <div class="message__wrapper">
                                    <div class="star-rating__wrapper" data-star="5">
                                        <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating">
                                        </label>
                                        <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating">
                                        </label>
                                        <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating">
                                        </label>
                                        <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating">
                                        </label>
                                        <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating">
                                        </label>
                                    </div>
                                    <p class="message">
                                        Повседневная практика показывает, что сложившаяся структура организации требуют определения и уточнения модели развития.
                                        <br>
                                        Задача организации, в особенности же укрепление и развитие структуры позволяет оценить значение соответствующий условий активизации. Не следует, однако забывать, что начало повседневной работы по формированию позиции позволяет выполнять важные задания по разработке дальнейших направлений развития.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="item">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-12 d-flex flex-column flex-md-column flex-sm-row align-items-md-start align-items-center justify-content-between">
                                <a class="avatar" href="#" style="background-image: url('../images/avatar1.png')">

                                </a>
                                <h2 class="initials">Beatriz Elorza, <b>architetta</b></h2>
                                <p class="date">17 мая</p>
                            </div>
                            <div class="col-lg-9 p-0 col-12 d-flex flex-column justify-content-between align-items-start">
                                <div class="message__wrapper">
                                    <div class="star-rating__wrapper" data-star="5">
                                        <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating">
                                        </label>
                                        <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating">
                                        </label>
                                        <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating">
                                        </label>
                                        <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating">
                                        </label>
                                        <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating">
                                        </label>
                                    </div>
                                    <p class="message">
                                        Повседневная практика показывает, что сложившаяся структура организации требуют определения и уточнения модели развития.
                                        <br>
                                        Задача организации, в особенности же укрепление и развитие структуры позволяет оценить значение соответствующий условий активизации. Не следует, однако забывать, что начало повседневной работы по формированию позиции позволяет выполнять важные задания по разработке дальнейших направлений развития.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="item">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-12 d-flex flex-column flex-md-column flex-sm-row align-items-md-start align-items-center justify-content-between">
                                <a class="avatar" href="#" style="background-image: url('../images/avatar1.png')">

                                </a>
                                <h2 class="initials">Beatriz Elorza, <b>architetta</b></h2>
                                <p class="date">17 мая</p>
                            </div>
                            <div class="col-lg-9 p-0 col-12 d-flex flex-column justify-content-between align-items-start">
                                <div class="message__wrapper">
                                    <div class="star-rating__wrapper" data-star="5">
                                        <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating">
                                        </label>
                                        <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating">
                                        </label>
                                        <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating">
                                        </label>
                                        <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating">
                                        </label>
                                        <label class="star-rating__ico star-rating__checked fad fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating">
                                        </label>
                                    </div>
                                    <p class="message">
                                        Повседневная практика показывает, что сложившаяся структура организации требуют определения и уточнения модели развития.
                                        <br>
                                        Задача организации, в особенности же укрепление и развитие структуры позволяет оценить значение соответствующий условий активизации. Не следует, однако забывать, что начало повседневной работы по формированию позиции позволяет выполнять важные задания по разработке дальнейших направлений развития.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row py-sm-5 my-sm-5">
                <div class="offset-lg-4 offset-sm-2"></div>
                <div class="col-lg-4 col-sm-8">
                    <a href="#" class="more">Читать все отзывы</a>
                </div>
                <div class="offset-lg-4 offset-sm-2"></div>
            </div>
        </div>
    </div>
    <div class="video">
        <div class="container">
            <div class="row py-5">
                <div class="col-lg-12 d-flex justify-content-center">
                    <figure class="video-wrapper">
                        <img src="./images/tv.png">
                        <div class="fluid-width-video-wrapper">
                            <video loop="" playsinline="" poster="./images/poster.png">
                                <source src="./videos/mov_bbb.mp4" type="video/mp4" codecs="avc1.42E01E, mp4a.40.2">
                            </video>
                            <button type="button" id="play-btn">
                                <div class="first-circle"></div>
                                <div class="second-circle"></div>
                            </button>
                        </div>
                        <figcaption>Смотрите <span>видео</span> о нашей компании</figcaption>
                    </figure>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once("footer.php");
?>