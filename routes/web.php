<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


/* main page router*/
Route::get('/', 'HomeController@index')->name('home');
/* main page router*/

/* profile page router*/
Route::get('/profile', 'ProfileController@index')->name('profile');
/* profile page router*/

/* request router */
Route::post('/request/create', 'RequestController@create');

Route::post('/request/support', 'RequestController@createSupport');

Route::post('/request/package', 'RequestController@packageRequest');
/* request router*/

// pages
Route::get('/about', 'AboutController@index')->name('about');
Route::get('/advantages', 'AdvantagesController@index')->name('advantages');
Route::get('/promotional', 'PromotionalController@index')->name('promotional');
Route::get('/treaty', 'TreatyController@index')->name('treaty');

Route::get('/show/{id}', 'PromotionalController@show')->name('show');



Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile/index', 'UserController@index')->middleware('auth:web');
Route::post('/password/update', 'UserController@password');
Route::post('/profile/update', 'UserController@update');

Route::post('/user/question', 'QuestionController@index');


Route::get('/test/pay', 'PayController@test');
Route::post('/package', 'PackageController@index')->name('package');
Route::get('/buy/{id}', 'PayController@index')->name('buy');

Route::get('/pay/result', 'PayController@result');
Route::get('/pay/success', 'PayController@success');
Route::get('/pay/failure', 'PayController@failure');


Route::post('/reviews/add', 'ReviewsController@add');
Route::get('/reviews', 'ReviewsController@index')->name('reviews');

Route::get('/brochures', 'BrochureController@index')->name('brochures');
