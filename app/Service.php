<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Service extends Model
{
    const CONSULTATION  = 1;
    const BROCHURE      = 2;
    const CONSULTATION_BROCHURE = 3;

    public static function getAll()
    {
        return self::get();
    }

    public static function getPackageServices($package_id)
    {
        return self::where('package_id', $package_id)->get();
    }

    public function packageService()
    {
        return $this->hasOne('App\PackageService', 'id', 'package_id');
    }
}
