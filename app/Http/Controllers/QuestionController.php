<?php

namespace App\Http\Controllers;

use App\UserQuestion;
use App\UsersService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class QuestionController extends Controller
{
    public function index(Request $request)
    {
        if (!Auth::check()) {
            return back()->with('unauthenticated', '1');
        }

        $validator  = Validator::make($request->all(), [
            'service'       => 'required|int',
            'theme'         => 'required|string',
            'question'      => 'required|string',
        ]);

        if($validator->fails()){
            $errors  = $validator->errors();
            return response([
                'message'   => $errors->first()
            ], 422);
        }else{
            $userServiceId  = $request->userServiceId;

            UserQuestion::create([
                'user_id'       => Auth::id(),
                'service_id'    => $request->service,
                'theme'         => $request->theme,
                'question'      => $request->question,
            ]);

            UsersService::useService($userServiceId);

            return response([
                'message'   =>
                    'Ваш вопрос получен и будет обработан! <br /> Ответ поступить на почту ' . Auth::user()->email,
            ], 200);
        }

    }
}