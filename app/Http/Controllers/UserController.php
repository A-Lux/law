<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        $user   = Auth::guard('web')->user();

        return $user;
    }

    public function update(Request $request)
    {
        if (!Auth::check()) {
            return back()->with('unauthenticated', '1');
        }

        $user   = Auth::user();

        $validator  = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
        ]);

        if ($validator->fails()) {
            $errors  = $validator->errors();
            return response([
                'status'    => 0,
                'message'   => $errors->first()
            ], 200);
        } else {
            if ($user->update(['name' => $request->name, 'phone' => $request->phone])) {
                return response([
                    'status' => 1,
                    'message' => 'Вы успешно обновили профиль'
                ], 200);
            }

            return response([
                'status' => 0,
                'message' => 'Ошибка сервера. Попробуйте позднее!'
            ], 500);
        }
    }


    public function password(Request $request)
    {
        if (!Auth::check()) {
            return back()->with('unauthenticated', '1');
        }

        $user   = Auth::user();

        $validator  = Validator::make($request->all(), [
            'password'      => ['required', 'string', 'min:8'],
            'new_password'  => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if ($validator->fails()) {
            $errors  = $validator->errors();
            return response([
                'status'    => 0,
                'message'   => $errors->first()
            ], 200);
        } else {
            if (\Hash::check($request->password, $user->password)) {
                if ($user->update(['password' => \Hash::make($request->password, ['rounds' => 12])])) {
                    return response([
                        'status' => 1,
                        'message' => 'Вы успешно обновили пароль'
                    ], 200);
                }
            } else {
                return response([
                    'status' => 0,
                    'message' => 'Старый пароль не правильный'
                ], 200);
            }
        }
    }
}
