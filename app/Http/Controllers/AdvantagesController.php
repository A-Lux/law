<?php

namespace App\Http\Controllers;

use App\About;
use App\Activity;
use App\Advantage;
use App\Banner;
use App\Project;
use App\Service;

class AdvantagesController extends Controller
{
    public function index()
    {
        $advantage  = Advantage::first();
        $banners    = Banner::where('menu_id', Banner::MENU_ADVANTAGES)
            ->get();

        return view('advantages.index', [
            'advantage'    => $advantage,
            'banners'       => $banners,
        ]);
    }
}