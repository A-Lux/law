<?php

namespace App\Http\Controllers;

use App\Order;
use App\UsersBuy;

class PayController extends Controller
{
    const ID_ORGANIZATION   = 528431;
    const SECRET_KEY        = 'gdFZLyM1z7tLpqYW';

    public function index($id)
    {
        $order    = Order::where('id', $id)->first();

        $request = [
            'pg_merchant_id'            => self::ID_ORGANIZATION,
            'pg_amount'                 => $order->sum,
            'pg_salt'                   => 'Zapne1Rt5Q6m',
            'pg_order_id'               => $order->id,
            'pg_description'            => 'Оплата заказа',
            'pg_user_phone'             => $order->user->phone,
            'pg_user_contact_email'     => $order->user->email,
            'pg_result_url'             => 'https://bz.com.kz/pay/result',
            'pg_success_url'            => 'https://bz.com.kz/pay/success',
            'pg_failure_url'            => 'https://bz.com.kz/pay/failure'
        ];


//        $request['pg_testing_mode'] = 1; //add this parameter to request for testing payments

        ksort($request);
        array_unshift($request, 'payment.php');
        array_push($request, self::SECRET_KEY);


        $request['pg_sig'] = md5(implode(';', $request));

        unset($request[0], $request[1]);

        $query = http_build_query($request);

        header('Location:https://api.paybox.money/payment.php?'.$query);
        die;
    }

    public function result()
    {
        if($_GET['pg_result'] == 1){
            $order = Order::where('id', $_GET['pg_order_id'])->first();
            mail('bukvazakona2020@gmail.com','Покупка с сайта','Имя '.$order->user->name.', Телефон '.$order->user->phone.' Покупка на сумму'.$order->sum,'From: webmaster@example.com' . "\r\n" .
                'Reply-To: webmaster@example.com' . "\r\n" .
                'X-Mailer: PHP/' . phpversion());
            $order->update(['status' => 1]);

        }else{
            $data['error'] = 'Ошибка оплаты!';
            echo json_encode($data);
        }
        die;
    }

    public function success()
    {
        $order = Order::where('id', $_GET['pg_order_id'])->first();
        $order->update(['status' => 1]);
        foreach ($order->userBuys as $userBuy){
            $userBuy->update(['status' => 1]);
        }

        if($order->status == 1){
            \Session::flash('success', 'Вы успешно оплатили пакет услуг!');
            return redirect('/profile');
        }
        \Session::flash('error', 'Оплата не прошла! Попробуйте позднее!');
        return redirect('/');
    }

    public function failure()
    {
        \Session::flash('error', 'Оплата не прошла! Попробуйте позднее!');
        return redirect('/');
    }

    public function test()
    {
        $request = [
            'pg_merchant_id'            => self::ID_ORGANIZATION,
            'pg_amount'                 => 123,
            'pg_salt'                   => 'dsQascv123Ghn',
            'pg_order_id'               => 123,
            'pg_description'            => 'Оплата заказа',
            'pg_user_phone'             => 123123123123,
            'pg_user_contact_email'     => 'test@test.com',
            'pg_result_url'             => 'http://law.loc/pay/result',
            'pg_success_url'            => 'http://law.loc/pay/success',
            'pg_failure_url'            => 'http://law.loc/pay/failure'
        ];

        $request['pg_testing_mode'] = 1; //add this parameter to request for testing payments

        ksort($request);
        array_unshift($request, 'payment.php');
        array_push($request, self::SECRET_KEY);


        $request['pg_sig'] = md5(implode(';', $request));

        unset($request[0], $request[1]);

        $query = http_build_query($request);

        header('Location:https://api.paybox.money/payment.php?'.$query);
        die;
    }
}
