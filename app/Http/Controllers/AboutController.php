<?php

namespace App\Http\Controllers;

use App\About;
use App\Activity;
use App\Advantage;
use App\Banner;
use App\Project;
use App\Service;

class AboutController extends Controller
{
    public function index()
    {
        $about      = About::first();
        $banners    = Banner::where('menu_id', Banner::MENU_ABOUT)
            ->get();

        return view('about.index', [
            'about'         => $about,
            'banners'       => $banners,
        ]);
    }
}