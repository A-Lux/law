<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Brochure;
use App\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BrochureController extends Controller
{
    public function index()
    {
        $banners    = Banner::where('menu_id', Banner::MENU_BROCHURES)
            ->get();
        $brochures  = Brochure::orderBy('created_at', 'desc')->get();

        return view('brochures.index', [
            'banners'       => $banners,
            'brochures'     => $brochures,
        ]);
    }
}