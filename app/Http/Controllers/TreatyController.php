<?php

namespace App\Http\Controllers;

use App\About;
use App\Activity;
use App\Advantage;
use App\Banner;
use App\Project;
use App\Service;
use App\Treaty;

class TreatyController extends Controller
{
    public function index()
    {
        $banners    = Banner::where('menu_id', Banner::MENU_TREATY)
            ->get();
        $treaties   = Treaty::get();

        return view('treaty.index', [
            'banners'   => $banners,
            'treaties'  => $treaties,
        ]);
    }
}