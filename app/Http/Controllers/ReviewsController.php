<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReviewsController extends Controller
{
    public function index()
    {
        $banners    = Banner::where('menu_id', Banner::MENU_REVIEWS)
            ->get();

        $reviews    = Review::orderby('created_at', 'desc')->get();

        return view('reviews.index', [
            'banners'       => $banners,
            'reviews'       => $reviews,
        ]);
    }

    public function add(Request $request)
    {
        $validator  = Validator::make($request->all(), [
            'username'  => 'required|string|max:255',
            'position'  => 'required|string|max:255',
            'message'   => 'required|string',
        ]);

        if($validator->fails()){
            $errors  = $validator->errors();
            return response([
                'message'   => $errors->first()
            ], 422);
        }else{
            $model  = Review::create([
                'username'  => $request->username,
                'position'  => $request->position,
                'content'   => $request->message,
            ]);

            return response([
                'message'   => 'Вы успешно оставили отзыв!'
            ], 200);
        }
    }
}