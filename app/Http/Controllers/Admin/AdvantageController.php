<?php

namespace App\Http\Controllers\Admin;

use App\About;
use App\Advantage;
use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class AdvantageController extends VoyagerBaseController
{
    public function index(Request $request)
    {
        return parent::show($request, Advantage::first()->id);
    }
}