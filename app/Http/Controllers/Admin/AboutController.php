<?php

namespace App\Http\Controllers\Admin;

use App\About;
use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class AboutController extends VoyagerBaseController
{
    public function index(Request $request)
    {
        return parent::show($request, About::first()->id);
    }
}
