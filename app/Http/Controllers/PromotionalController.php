<?php

namespace App\Http\Controllers;

use App\About;
use App\Activity;
use App\Advantage;
use App\Banner;
use App\PackageService;
use App\Recommendation;
use App\Service;
use App\UsersBuy;
use App\UsersService;
use Illuminate\Support\Facades\Auth;

class PromotionalController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $banners            = Banner::where('menu_id', Banner::MENU_PROMOTIONAL)
                                    ->get();

        $recommendations    = Recommendation::get();

        return view('promotional.index', [
            'banners'           => $banners,
            'recommendations'   => $recommendations,
        ]);
    }

    public function show($id)
    {
        $banners        = Banner::where('menu_id', Banner::MENU_PACKAGE)
            ->get();

        $package        = PackageService::where('id', $id)
            ->first();

        $services       = Service::where('package_id', $package->id)
            ->get();

        return view('promotional.show', [
            'banners'   => $banners,
            'package'   => $package,
            'services'  => $services,
        ]);
    }
}
