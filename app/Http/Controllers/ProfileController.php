<?php

namespace App\Http\Controllers;

use App\About;
use App\Advantage;
use App\Banner;
use App\PackageService;
use App\Service;
use App\UsersBuy;
use App\UsersService;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (!Auth::check()) {
            return back()->with('unauthenticated', '1');
        }

        $buys           = UsersBuy::getUserPackages(Auth::user()->id);

        $consultations  = UsersBuy::listPackages(Auth::user()->id);

        $banners        = Banner::where('menu_id', Banner::MENU_PROMOTIONAL)
            ->get();
        $user           = [
            'name'      => Auth::user()->name,
            'phone'     => Auth::user()->phone,
            'email'     => Auth::user()->email,
        ];

        return view('profile.index', [
            'banners'   => $banners,
            'buys'      => $buys,
            'user'      => $user,
            'consultations' => $consultations,
        ]);
    }

    public function show($id)
    {
        $banners        = Banner::where('menu_id', Banner::MENU_PROMOTIONAL)
            ->get();

        $package        = PackageService::where('id', $id)
            ->first();

        $services       = Service::where('package_id', $package->id)
            ->get();

        return view('promotional.show', [
            'banners'   => $banners,
            'package'   => $package,
            'services'  => $services,
        ]);
    }
}