<?php

namespace App\Http\Controllers;

use App\About;
use App\Banner;
use App\Brochure;
use App\PackageService;
use App\Review;
use App\Service;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $about      = About::first();
        $banners    = Banner::where('menu_id', Banner::MENU_MAIN)
            ->get();

        $services   = PackageService::getAll();
        $brochures  = Brochure::orderBy('created_at', 'desc')->limit(3)->get();
        $reviews    = Review::orderby('created_at', 'desc')->get();

        return view('home.index', [
            'about'         => $about,
            'banners'       => $banners,
            'services'      => $services,
            'reviews'       => $reviews,
            'brochures'     => $brochures,
        ]);
    }
}
