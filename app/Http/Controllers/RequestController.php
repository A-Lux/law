<?php

namespace App\Http\Controllers;

use App\Request as RequestUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RequestController extends Controller
{
    public function create(Request $request)
    {
        $validator  = Validator::make($request->all(), [
            'name'  => 'required|string|max:255',
            'phone' => 'required|string|max:255',
        ]);

        if($validator->fails()){
            $errors  = $validator->errors();
            return response([
                'status'    => 0,
                'message'   => $errors->first()
            ], 200);
        }else{
            $model  = RequestUser::create([
                'name'  => $request->name,
                'phone' => $request->phone
            ]);
            if(mail('bukvazakona2020@gmail.com','Заявка с сайта','Имя '.$request->name.', Телефон '.$request->phone,'From: webmaster@example.com' . "\r\n" .
                'Reply-To: webmaster@example.com' . "\r\n" .
                'X-Mailer: PHP/' . phpversion()))
                return response([
                    'status'    => 1,
                    'message'   => 'Ваша заявка принята'
                ], 200);
        }
    }

    public function createSupport(Request $request)
    {
        $validator  = Validator::make($request->all(), [
            'name'      => 'required|string|max:255',
            'phone'     => 'required|string|max:255',
            'message'   => 'string',
        ]);

        if($validator->fails()){
            $errors  = $validator->errors();
            return response([
                'status'    => 0,
                'message'   => $errors->first()
            ], 200);
        }else{
            $model  = RequestUser::create([
                'name'      => $request->name,
                'phone'     => $request->phone,
                'message'   => $request->message
            ]);

            return response([
                'status'    => 1,
                'message'   => 'Ваша заявка принята'
            ], 200);
        }
    }

    public function packageRequest(Request $request)
    {
        $validator  = Validator::make($request->all(), [
            'name'      => 'required|string|max:255',
            'phone'     => 'required|string|max:255',
            'message'   => 'string',
        ]);

        if($validator->fails()){
            $errors  = $validator->errors();
            return response([
                'status'    => 0,
                'message'   => $errors->first()
            ], 200);
        }else{
            $model  = RequestUser::create([
                'name'      => $request->name,
                'phone'     => $request->phone,
                'message'   => $request->message
            ]);

            return response([
                'status'    => 1,
                'message'   => 'Ваша заявка принята'
            ], 200);
        }
    }
}
