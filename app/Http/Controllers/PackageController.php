<?php

namespace App\Http\Controllers;

use App\Order;
use App\PackageService;
use App\Service;
use App\UserBonus;
use App\UsersBuy;
use App\UsersService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PackageController extends Controller
{
    public function index(Request $request)
    {
        if (!Auth::check()) {
            return back()->with('unauthenticated', '1');
        }

        $package    = PackageService::where('id', $request->package_id)->first();
        $services   = Service::getPackageServices($package->id);

        $order  = Order::create([
            'user_id'       => Auth::id(),
            'package_id'    => $request->package_id,
            'status'        => Order::NOT_PAID,
            'count'         => $request->count,
            'sum'           => $request->sum,
        ]);

        for ($i = 1; $i <= $request->count; $i++){
            $this->addBonus($package);

            $buy        = UsersBuy::create([
                'order_id'      => $order->id,
                'user_id'       => Auth::id(),
                'package_id'    => $package->id,
                'status'        => UsersBuy::NOT_PAID,
            ]);

            $this->addServices($services, $buy);
        }

        return response([
            'order_id'  => $order->id,
        ]);
    }

    public function addServices($services, $buy)
    {
        $temp = [];
        foreach ($services as $service){
            $temp[] = [
                'buy_id'        => $buy->id,
                'service_id'    => $service->id,
                'count'         => $service->total,
                'date'          => Carbon::now()->addYears($service->expiration_date),
            ];
        }

        UsersService::insert($temp);
    }

    protected function addBonus($package)
    {
        $bonusUser = UserBonus::where('user_id', Auth::id())->first();
        if($bonusUser){
            $bonus  = $bonusUser->bonus + $package->bonus;
            $sale   = $bonusUser->sale  + $package->sale;

            $bonusUser->update(['bonus' => $bonus, 'sale' => $sale]);
        }else{
            $bonusUser  = UserBonus::create([
                'user_id'   => Auth::id(),
                'bonus'     => $package->bonus != null ? $package->bonus : 0,
                'sale'      => $package->sale != null ? $package->sale : 0,
            ]);
        }

        return $bonusUser;
    }

    public function test()
    {
        $order  = Order::where('id', 2)->first();

//        foreach ($order->userBuys as $userBuy){
//            $userBuy->update(['status' => 1]);
//        }
        dd($order->user);
    }
}
