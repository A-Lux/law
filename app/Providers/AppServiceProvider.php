<?php

namespace App\Providers;

use App\Document;
use App\SocialNetwork;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function ($view) {
            $models = Document::all()->keyBy('name');

            $view->with('documents', $models);
        });

        view()->composer(['layouts._footer'], function ($view) {
            $socials    = SocialNetwork::get();

            return $view->with([
                'socials' => $socials,
            ]);
        });


    }
}
