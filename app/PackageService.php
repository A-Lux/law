<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PackageService extends Model
{
    const TYPE_ADVICE               = 1;
    const TYPE_BROCHURE             = 2;

    public static function getAll()
    {
        return self::orderBy('sort', 'asc')->get();
    }
}
