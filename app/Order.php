<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
    const NOT_PAID  = 0;
    const PAID      = 1;

    protected $fillable = ['user_id', 'package_id', 'status', 'sum', 'count'];

    public function package()
    {
        return $this->hasOne('App\PackageService', 'id', 'package_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function userBuys()
    {
        return $this->hasMany('App\UsersBuy', 'order_id','id');
    }
}
