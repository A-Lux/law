<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class UsersService extends Model
{
    protected $fillable = ['buy_id', 'service_id', 'count', 'date'];

    public function service()
    {
        return $this->hasOne('App\Services', 'id', 'service_id');
    }

    public static function getUserServices($buy_id)
    {
        $temp = [];
        $items = self::where('buy_id', $buy_id)
            ->join('services', 'service_id', 'services.id')
            ->get()
            ->groupBy('package_id');

        foreach ($items as $key => $item) {
            $temp[] = [
                'id' => $key,
                'name' => PackageService::where('id', $key)->select('name')->first()->name,
                'items' => $item,
            ];
        }

        return $temp;
    }

    public static function listConsultation($buy_id)
    {
        $services   = self::where('buy_id', $buy_id)->get();
        $list   = [];

        foreach ($services as $service){
            $query      = Service::where('id', $service->service_id)
                ->first();

            if($query->type == Service::CONSULTATION || $query->type == Service::CONSULTATION_BROCHURE) {
                if($service->count > 0) {
                    $list[] = [
                        'id' => $service->id,
                        'service_id' => $query->id,
                        'name' => $query->name . ' ( ' . $query->packageService->name . ' )',
                    ];
                }
            }
        }


        return $list;
    }

    public static function useService($userServiceId)
    {
        $userService    = UsersService::where('id', $userServiceId)->first();

        $userService->increment('render', 1);
        $userService->decrement('count', 1);

        return response([], 200);
    }
}
