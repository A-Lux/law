<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class UsersBuy extends Model
{
    const NOT_PAID  = 0;
    const PAID      = 1;

    protected $fillable = ['user_id', 'package_id', 'status', 'order_id'];

    public static function getUserPackages($user_id)
    {
        $buys            = self::where([
            ['user_id', '=', $user_id],
            ['status', '=', 1],
        ])->get();
        $userServices  = [];

        foreach ($buys as $buy){
            $userServices    = array_merge(UsersService::getUserServices($buy->id), $userServices);
        }
        return $userServices;
    }

    public static function listPackages($user_id)
    {
        $buys   = self::where('user_id', $user_id)->get();
        $list   = [];

        foreach ($buys as $buy) {
            $list = array_merge(UsersService::listConsultation($buy->id), $list);
        }

        return $list;

    }

    public function package()
    {
        return $this->hasOne('App\PackageService', 'id', 'package_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function order()
    {
        return $this->hasOne('App\Order', 'id', 'order_id');
    }
}
