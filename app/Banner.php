<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Banner extends Model
{
    const MENU_MAIN             = 1;
    const MENU_ABOUT            = 2;
    const MENU_ADVANTAGES       = 3;
    const MENU_TREATY           = 4;
    const MENU_PROMOTIONAL      = 5;
    const MENU_PROFILE          = 6;
    const MENU_PACKAGE          = 7;
    const MENU_REVIEWS          = 8;
    const MENU_BROCHURES        = 9;

    public static function getAll()
    {
        return self::get();
    }
}
