-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 27 2020 г., 14:16
-- Версия сервера: 5.7.20
-- Версия PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `law`
--

-- --------------------------------------------------------

--
-- Структура таблицы `about`
--

CREATE TABLE `about` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `subtitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `about`
--

INSERT INTO `about` (`id`, `title`, `content`, `subtitle`, `video`, `created_at`, `updated_at`) VALUES
(1, 'О компании', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam semper sed turpis sed sollicitudin. Aliquam vitae aliquam eros. Nunc nec posuere diam, ut congue arcu. Vivamus egestas ipsum at metus varius dictum. Pellentesque vehicula blandit lacus ac aliquet. Aenean luctus nulla nec fermentum consequat. Praesent elementum urna lectus, vitae vestibulum odio imperdiet eget. Praesent et lectus dapibus, placerat ante vitae, porttitor diam. Quisque ullamcorper lacinia efficitur. Donec vel quam tincidunt, imperdiet ante fringilla, tincidunt elit.</p>                         <p>Vivamus bibendum venenatis ornare. Vivamus pretium ligula leo, at sagittis diam iaculis sit amet. Phasellus sit amet blandit sem. Ut non nulla efficitur, luctus velit a, molestie dui. Donec iaculis tristique ipsum, sed laoreet massa rhoncus eget. Phasellus vestibulum sit amet nulla sit amet tristique. Curabitur semper imperdiet mollis.</p>                         <p>Vivamus sapien sapien, ullamcorper lobortis metus vel, fermentum accumsan nisi. Integer egestas mi tincidunt diam congue, quis egestas urna cursus. Curabitur nunc velit, pharetra varius ligula eget, blandit efficitur arcu. Nam volutpat ante sed semper varius. In lacinia pretium dui, tincidunt efficitur magna gravida et. Sed maximus lacus dictum, malesuada lorem et, imperdiet erat. Etiam felis ante, rhoncus eu risus non, accumsan sagittis eros. Phasellus sed malesuada leo. Donec pretium blandit scelerisque. Donec eros arcu, consectetur in nibh nec, elementum hendrerit lorem. In sed leo non</p>', 'Смотрите <span>видео</span> о нашей компании', NULL, '2020-04-27 10:42:00', '2020-04-27 04:42:42');

-- --------------------------------------------------------

--
-- Структура таблицы `advantages`
--

CREATE TABLE `advantages` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `statusButton` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `brochures`
--

CREATE TABLE `brochures` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `file` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2020-03-27 01:51:24', '2020-03-27 01:51:24'),
(2, NULL, 1, 'Category 2', 'category-2', '2020-03-27 01:51:24', '2020-03-27 01:51:24');

-- --------------------------------------------------------

--
-- Структура таблицы `components`
--

CREATE TABLE `components` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(23, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(24, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(25, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(26, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(27, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(30, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
(31, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(33, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
(34, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
(35, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(39, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(42, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
(43, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(45, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(46, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(47, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(48, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(49, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(50, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(52, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(53, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(54, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(55, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(56, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(57, 7, 'title', 'text', 'Заголовок', 1, 1, 1, 1, 1, 1, '{}', 2),
(58, 7, 'content', 'rich_text_box', 'Контент', 0, 1, 1, 1, 1, 1, '{}', 3),
(59, 7, 'image', 'image', 'Изображение', 0, 1, 1, 1, 1, 1, '{}', 4),
(60, 7, 'link', 'text', 'Ссылка', 0, 1, 1, 1, 1, 1, '{}', 5),
(61, 7, 'statusButton', 'select_dropdown', 'Статус кнопки', 1, 1, 1, 1, 1, 1, '{\"default\":\"0\",\"options\":{\"0\":\"\\u041d\\u0435 \\u043f\\u043e\\u043a\\u0430\\u0437\\u044b\\u0432\\u0430\\u0442\\u044c\",\"1\":\"\\u0410\\u043a\\u0442\\u0438\\u0432\\u043d\\u044b\\u0439\"}}', 6),
(62, 7, 'created_at', 'timestamp', 'Дата создания', 0, 1, 1, 1, 0, 1, '{}', 7),
(63, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(64, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(65, 8, 'title', 'text', 'Наименование', 1, 1, 1, 1, 1, 1, '{}', 2),
(66, 8, 'image', 'image', 'Изображение', 0, 1, 1, 1, 1, 1, '{}', 3),
(67, 8, 'created_at', 'timestamp', 'Дата создания', 0, 1, 1, 1, 0, 1, '{}', 4),
(68, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(69, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(70, 9, 'title', 'text', 'Наименование', 1, 1, 1, 1, 1, 1, '{}', 2),
(71, 9, 'image', 'image', 'Изображение', 0, 1, 1, 1, 1, 1, '{}', 3),
(72, 9, 'file', 'file', 'Файл', 0, 1, 1, 1, 1, 1, '{}', 4),
(73, 9, 'created_at', 'timestamp', 'Дата создания', 0, 1, 1, 1, 0, 1, '{}', 5),
(74, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(75, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(76, 10, 'title', 'text', 'Заголовок', 1, 1, 1, 1, 1, 1, '{}', 2),
(77, 10, 'content', 'text', 'Контент', 0, 1, 1, 1, 1, 1, '{}', 3),
(78, 10, 'subtitle', 'text', 'Подзаголовок', 0, 1, 1, 1, 1, 1, '{}', 4),
(79, 10, 'video', 'text', 'Видео', 0, 1, 1, 1, 1, 1, '{}', 5),
(80, 10, 'created_at', 'timestamp', 'Дата создания', 0, 1, 1, 1, 0, 1, '{}', 6),
(81, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(82, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(83, 11, 'text', 'text', 'Текст', 1, 1, 1, 1, 1, 1, '{}', 2),
(84, 11, 'image', 'image', 'Изображение', 0, 1, 1, 1, 1, 1, '{}', 3),
(85, 11, 'created_at', 'timestamp', 'Дата создания', 0, 1, 1, 1, 0, 1, '{}', 4),
(86, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(87, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(88, 12, 'name', 'text', 'Имя', 1, 1, 1, 1, 1, 1, '{}', 2),
(89, 12, 'phone', 'text', 'Телефон', 1, 1, 1, 1, 1, 1, '{}', 3),
(90, 12, 'created_at', 'timestamp', 'Дата создания', 0, 1, 1, 1, 0, 1, '{}', 4),
(91, 12, 'updated_at', 'timestamp', 'Дата изменения', 0, 0, 0, 0, 0, 0, '{}', 5),
(92, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(93, 13, 'username', 'text', 'Имя Фамилия', 1, 1, 1, 1, 1, 1, '{}', 2),
(94, 13, 'position', 'text', 'Должность', 0, 1, 1, 1, 1, 1, '{}', 3),
(95, 13, 'content', 'text', 'Отзыв', 1, 1, 1, 1, 1, 1, '{}', 4),
(96, 13, 'rating', 'text', 'Оценка', 0, 1, 1, 1, 1, 1, '{}', 5),
(97, 13, 'image', 'text', 'Изображение', 0, 1, 1, 1, 1, 1, '{}', 6),
(98, 13, 'created_at', 'timestamp', 'Дата создания', 0, 1, 1, 1, 0, 1, '{}', 7),
(99, 13, 'updated_at', 'timestamp', 'Дата изменения', 0, 0, 0, 0, 0, 0, '{}', 8),
(100, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(101, 14, 'title', 'text', 'Заголовок', 1, 1, 1, 1, 1, 1, '{}', 2),
(102, 14, 'content', 'rich_text_box', 'Контент', 0, 1, 1, 1, 1, 1, '{}', 3),
(103, 14, 'created_at', 'timestamp', 'Дата создания', 0, 1, 1, 1, 0, 1, '{}', 4),
(104, 14, 'updated_at', 'timestamp', 'Дата изменения', 0, 0, 0, 0, 0, 0, '{}', 5),
(105, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(106, 15, 'name', 'text', 'Ключ', 1, 1, 1, 0, 1, 0, '{}', 2),
(107, 15, 'text', 'text', 'Текст', 0, 1, 1, 1, 1, 1, '{}', 3),
(108, 15, 'image', 'text', 'Изображение', 0, 1, 1, 1, 1, 1, '{}', 4),
(109, 15, 'created_at', 'timestamp', 'Дата создания', 0, 1, 1, 1, 0, 1, '{}', 5),
(110, 15, 'updated_at', 'timestamp', 'Дата изменения', 0, 0, 0, 0, 0, 0, '{}', 6),
(111, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(112, 16, 'name', 'text', 'Ключ', 1, 1, 1, 0, 1, 0, '{}', 2),
(113, 16, 'text', 'text', 'Текст', 0, 1, 1, 1, 1, 1, '{}', 3),
(114, 16, 'file', 'text', 'Файл', 0, 1, 1, 1, 1, 1, '{}', 4),
(115, 16, 'created_at', 'timestamp', 'Дата создания', 0, 1, 1, 1, 0, 1, '{}', 5),
(116, 16, 'updated_at', 'timestamp', 'Дата изменения', 0, 0, 0, 0, 0, 0, '{}', 6),
(117, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(118, 17, 'link', 'text', 'Ссылка', 1, 1, 1, 1, 1, 1, '{}', 2),
(119, 17, 'image', 'image', 'Изображение', 0, 1, 1, 1, 1, 1, '{}', 3),
(120, 17, 'created_at', 'timestamp', 'Дата создания', 0, 1, 1, 1, 0, 1, '{}', 4),
(121, 17, 'updated_at', 'timestamp', 'Дата изменения', 0, 0, 0, 0, 0, 0, '{}', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2020-03-27 01:51:24', '2020-03-27 01:51:24'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2020-03-27 01:51:24', '2020-03-27 01:51:24'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(7, 'banners', 'banners', 'Баннер', 'Баннеры', NULL, 'App\\Banner', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-03-29 22:43:16', '2020-03-29 22:50:54'),
(8, 'services', 'services', 'Услуга', 'Услуги', NULL, 'App\\Service', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-03-29 22:54:16', '2020-03-29 22:54:16'),
(9, 'brochures', 'brochures', 'Брошюра', 'Брошюры', NULL, 'App\\Brochure', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-03-29 22:59:19', '2020-03-29 22:59:19'),
(10, 'about', 'about', 'О компании', 'О компании', NULL, 'App\\About', NULL, 'App\\Http\\Controllers\\Admin\\AboutController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-04-27 01:03:08', '2020-04-27 03:28:56'),
(11, 'advantages', 'advantages', 'Преимущество', 'Преимущества', NULL, 'App\\Advantage', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-04-27 01:09:46', '2020-04-27 01:09:46'),
(12, 'requests', 'requests', 'Заявка', 'Заявки', NULL, 'App\\Request', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-04-27 01:14:36', '2020-04-27 01:14:36'),
(13, 'reviews', 'reviews', 'Отзыв', 'Отзывы', NULL, 'App\\Review', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-04-27 01:20:15', '2020-04-27 01:20:15'),
(14, 'treaties', 'treaties', 'Договор', 'Наш опыт', NULL, 'App\\Treaty', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-04-27 04:05:15', '2020-04-27 04:05:15'),
(15, 'components', 'components', 'Компонент', 'Компоненты', NULL, 'App\\Component', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-04-27 04:08:03', '2020-04-27 04:27:47'),
(16, 'documents', 'documents', 'Документ', 'Документы', NULL, 'App\\Document', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-04-27 04:10:28', '2020-04-27 04:27:55'),
(17, 'social_networks', 'social-networks', 'Социальная сеть', 'Социальные сети', NULL, 'App\\SocialNetwork', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-04-27 04:30:21', '2020-04-27 04:30:21');

-- --------------------------------------------------------

--
-- Структура таблицы `documents`
--

CREATE TABLE `documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `file` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(2, 'site', '2020-04-27 04:39:10', '2020-04-27 04:39:10');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-03-27 01:51:20', '2020-03-27 01:51:20', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, 26, 1, '2020-03-27 01:51:20', '2020-04-27 04:38:21', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2020-03-27 01:51:20', '2020-03-27 01:51:20', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-03-27 01:51:20', '2020-03-27 01:51:20', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 5, '2020-03-27 01:51:20', '2020-04-27 04:38:24', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, NULL, 7, '2020-03-27 01:51:20', '2020-04-27 04:41:03', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 1, '2020-03-27 01:51:20', '2020-04-27 04:41:03', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 2, '2020-03-27 01:51:20', '2020-04-27 04:41:03', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 3, '2020-03-27 01:51:20', '2020-04-27 04:41:03', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 6, '2020-03-27 01:51:20', '2020-04-27 04:38:24', 'voyager.settings.index', NULL),
(11, 1, 'Categories', '', '_self', 'voyager-categories', NULL, 26, 4, '2020-03-27 01:51:24', '2020-04-27 04:38:26', 'voyager.categories.index', NULL),
(12, 1, 'Posts', '', '_self', 'voyager-news', NULL, 26, 2, '2020-03-27 01:51:24', '2020-04-27 04:38:22', 'voyager.posts.index', NULL),
(13, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, 26, 3, '2020-03-27 01:51:25', '2020-04-27 04:38:26', 'voyager.pages.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 4, '2020-03-27 01:51:25', '2020-04-27 04:41:03', 'voyager.hooks', NULL),
(15, 1, 'Баннеры', '', '_self', NULL, NULL, NULL, 8, '2020-03-29 22:43:16', '2020-04-27 04:41:03', 'voyager.banners.index', NULL),
(16, 1, 'Услуги', '', '_self', NULL, NULL, NULL, 9, '2020-03-29 22:54:16', '2020-04-27 04:41:03', 'voyager.services.index', NULL),
(17, 1, 'Брошюры', '', '_self', NULL, NULL, NULL, 10, '2020-03-29 22:59:19', '2020-04-27 04:41:03', 'voyager.brochures.index', NULL),
(18, 1, 'О компании', '', '_self', NULL, NULL, NULL, 11, '2020-04-27 01:03:08', '2020-04-27 04:41:03', 'voyager.about.index', NULL),
(19, 1, 'Преимущества', '', '_self', NULL, NULL, NULL, 12, '2020-04-27 01:09:46', '2020-04-27 04:41:03', 'voyager.advantages.index', NULL),
(20, 1, 'Заявки', '', '_self', NULL, NULL, NULL, 13, '2020-04-27 01:14:36', '2020-04-27 04:41:03', 'voyager.requests.index', NULL),
(21, 1, 'Отзывы', '', '_self', NULL, NULL, NULL, 14, '2020-04-27 01:20:15', '2020-04-27 04:41:03', 'voyager.reviews.index', NULL),
(22, 1, 'Наш опыт', '', '_self', NULL, NULL, NULL, 15, '2020-04-27 04:05:15', '2020-04-27 04:41:03', 'voyager.treaties.index', NULL),
(23, 1, 'Компоненты', '', '_self', NULL, NULL, NULL, 16, '2020-04-27 04:08:03', '2020-04-27 04:41:03', 'voyager.components.index', NULL),
(24, 1, 'Документы', '', '_self', NULL, NULL, NULL, 17, '2020-04-27 04:10:29', '2020-04-27 04:41:03', 'voyager.documents.index', NULL),
(25, 1, 'Социальные сети', '', '_self', NULL, NULL, NULL, 18, '2020-04-27 04:30:21', '2020-04-27 04:41:03', 'voyager.social-networks.index', NULL),
(26, 1, 'Дополнительно', '', '_self', 'voyager-window-list', '#000000', NULL, 4, '2020-04-27 04:38:04', '2020-04-27 04:38:16', NULL, ''),
(27, 2, 'О компании', '/about', '_self', NULL, '#000000', NULL, 18, '2020-04-27 04:40:04', '2020-04-27 04:40:04', NULL, ''),
(28, 2, 'Наши преимущества', '/advantages', '_self', NULL, '#000000', NULL, 19, '2020-04-27 04:40:14', '2020-04-27 04:40:14', NULL, ''),
(29, 2, 'Наш опыт', '/treaty', '_self', NULL, '#000000', NULL, 20, '2020-04-27 04:40:30', '2020-04-27 04:57:59', NULL, ''),
(30, 2, 'Рекомендации', '/promotional', '_self', NULL, '#000000', NULL, 21, '2020-04-27 04:40:42', '2020-04-27 04:40:42', NULL, '');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2016_01_01_000000_create_pages_table', 2),
(24, '2016_01_01_000000_create_posts_table', 2),
(25, '2016_02_15_204651_create_categories_table', 2),
(26, '2017_04_11_000000_alter_post_nullable_fields_table', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2020-03-27 01:51:25', '2020-03-27 01:51:25');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(2, 'browse_bread', NULL, '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(3, 'browse_database', NULL, '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(4, 'browse_media', NULL, '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(5, 'browse_compass', NULL, '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(6, 'browse_menus', 'menus', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(7, 'read_menus', 'menus', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(8, 'edit_menus', 'menus', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(9, 'add_menus', 'menus', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(10, 'delete_menus', 'menus', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(11, 'browse_roles', 'roles', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(12, 'read_roles', 'roles', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(13, 'edit_roles', 'roles', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(14, 'add_roles', 'roles', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(15, 'delete_roles', 'roles', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(16, 'browse_users', 'users', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(17, 'read_users', 'users', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(18, 'edit_users', 'users', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(19, 'add_users', 'users', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(20, 'delete_users', 'users', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(21, 'browse_settings', 'settings', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(22, 'read_settings', 'settings', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(23, 'edit_settings', 'settings', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(24, 'add_settings', 'settings', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(25, 'delete_settings', 'settings', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(26, 'browse_categories', 'categories', '2020-03-27 01:51:24', '2020-03-27 01:51:24'),
(27, 'read_categories', 'categories', '2020-03-27 01:51:24', '2020-03-27 01:51:24'),
(28, 'edit_categories', 'categories', '2020-03-27 01:51:24', '2020-03-27 01:51:24'),
(29, 'add_categories', 'categories', '2020-03-27 01:51:24', '2020-03-27 01:51:24'),
(30, 'delete_categories', 'categories', '2020-03-27 01:51:24', '2020-03-27 01:51:24'),
(31, 'browse_posts', 'posts', '2020-03-27 01:51:24', '2020-03-27 01:51:24'),
(32, 'read_posts', 'posts', '2020-03-27 01:51:24', '2020-03-27 01:51:24'),
(33, 'edit_posts', 'posts', '2020-03-27 01:51:24', '2020-03-27 01:51:24'),
(34, 'add_posts', 'posts', '2020-03-27 01:51:24', '2020-03-27 01:51:24'),
(35, 'delete_posts', 'posts', '2020-03-27 01:51:24', '2020-03-27 01:51:24'),
(36, 'browse_pages', 'pages', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(37, 'read_pages', 'pages', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(38, 'edit_pages', 'pages', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(39, 'add_pages', 'pages', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(40, 'delete_pages', 'pages', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(41, 'browse_hooks', NULL, '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(42, 'browse_banners', 'banners', '2020-03-29 22:43:16', '2020-03-29 22:43:16'),
(43, 'read_banners', 'banners', '2020-03-29 22:43:16', '2020-03-29 22:43:16'),
(44, 'edit_banners', 'banners', '2020-03-29 22:43:16', '2020-03-29 22:43:16'),
(45, 'add_banners', 'banners', '2020-03-29 22:43:16', '2020-03-29 22:43:16'),
(46, 'delete_banners', 'banners', '2020-03-29 22:43:16', '2020-03-29 22:43:16'),
(47, 'browse_services', 'services', '2020-03-29 22:54:16', '2020-03-29 22:54:16'),
(48, 'read_services', 'services', '2020-03-29 22:54:16', '2020-03-29 22:54:16'),
(49, 'edit_services', 'services', '2020-03-29 22:54:16', '2020-03-29 22:54:16'),
(50, 'add_services', 'services', '2020-03-29 22:54:16', '2020-03-29 22:54:16'),
(51, 'delete_services', 'services', '2020-03-29 22:54:16', '2020-03-29 22:54:16'),
(52, 'browse_brochures', 'brochures', '2020-03-29 22:59:19', '2020-03-29 22:59:19'),
(53, 'read_brochures', 'brochures', '2020-03-29 22:59:19', '2020-03-29 22:59:19'),
(54, 'edit_brochures', 'brochures', '2020-03-29 22:59:19', '2020-03-29 22:59:19'),
(55, 'add_brochures', 'brochures', '2020-03-29 22:59:19', '2020-03-29 22:59:19'),
(56, 'delete_brochures', 'brochures', '2020-03-29 22:59:19', '2020-03-29 22:59:19'),
(57, 'browse_about', 'about', '2020-04-27 01:03:08', '2020-04-27 01:03:08'),
(58, 'read_about', 'about', '2020-04-27 01:03:08', '2020-04-27 01:03:08'),
(59, 'edit_about', 'about', '2020-04-27 01:03:08', '2020-04-27 01:03:08'),
(60, 'add_about', 'about', '2020-04-27 01:03:08', '2020-04-27 01:03:08'),
(61, 'delete_about', 'about', '2020-04-27 01:03:08', '2020-04-27 01:03:08'),
(62, 'browse_advantages', 'advantages', '2020-04-27 01:09:46', '2020-04-27 01:09:46'),
(63, 'read_advantages', 'advantages', '2020-04-27 01:09:46', '2020-04-27 01:09:46'),
(64, 'edit_advantages', 'advantages', '2020-04-27 01:09:46', '2020-04-27 01:09:46'),
(65, 'add_advantages', 'advantages', '2020-04-27 01:09:46', '2020-04-27 01:09:46'),
(66, 'delete_advantages', 'advantages', '2020-04-27 01:09:46', '2020-04-27 01:09:46'),
(67, 'browse_requests', 'requests', '2020-04-27 01:14:36', '2020-04-27 01:14:36'),
(68, 'read_requests', 'requests', '2020-04-27 01:14:36', '2020-04-27 01:14:36'),
(69, 'edit_requests', 'requests', '2020-04-27 01:14:36', '2020-04-27 01:14:36'),
(70, 'add_requests', 'requests', '2020-04-27 01:14:36', '2020-04-27 01:14:36'),
(71, 'delete_requests', 'requests', '2020-04-27 01:14:36', '2020-04-27 01:14:36'),
(72, 'browse_reviews', 'reviews', '2020-04-27 01:20:15', '2020-04-27 01:20:15'),
(73, 'read_reviews', 'reviews', '2020-04-27 01:20:15', '2020-04-27 01:20:15'),
(74, 'edit_reviews', 'reviews', '2020-04-27 01:20:15', '2020-04-27 01:20:15'),
(75, 'add_reviews', 'reviews', '2020-04-27 01:20:15', '2020-04-27 01:20:15'),
(76, 'delete_reviews', 'reviews', '2020-04-27 01:20:15', '2020-04-27 01:20:15'),
(77, 'browse_treaties', 'treaties', '2020-04-27 04:05:15', '2020-04-27 04:05:15'),
(78, 'read_treaties', 'treaties', '2020-04-27 04:05:15', '2020-04-27 04:05:15'),
(79, 'edit_treaties', 'treaties', '2020-04-27 04:05:15', '2020-04-27 04:05:15'),
(80, 'add_treaties', 'treaties', '2020-04-27 04:05:15', '2020-04-27 04:05:15'),
(81, 'delete_treaties', 'treaties', '2020-04-27 04:05:15', '2020-04-27 04:05:15'),
(82, 'browse_components', 'components', '2020-04-27 04:08:03', '2020-04-27 04:08:03'),
(83, 'read_components', 'components', '2020-04-27 04:08:03', '2020-04-27 04:08:03'),
(84, 'edit_components', 'components', '2020-04-27 04:08:03', '2020-04-27 04:08:03'),
(85, 'add_components', 'components', '2020-04-27 04:08:03', '2020-04-27 04:08:03'),
(86, 'delete_components', 'components', '2020-04-27 04:08:03', '2020-04-27 04:08:03'),
(87, 'browse_documents', 'documents', '2020-04-27 04:10:28', '2020-04-27 04:10:28'),
(88, 'read_documents', 'documents', '2020-04-27 04:10:29', '2020-04-27 04:10:29'),
(89, 'edit_documents', 'documents', '2020-04-27 04:10:29', '2020-04-27 04:10:29'),
(90, 'add_documents', 'documents', '2020-04-27 04:10:29', '2020-04-27 04:10:29'),
(91, 'delete_documents', 'documents', '2020-04-27 04:10:29', '2020-04-27 04:10:29'),
(92, 'browse_social_networks', 'social_networks', '2020-04-27 04:30:21', '2020-04-27 04:30:21'),
(93, 'read_social_networks', 'social_networks', '2020-04-27 04:30:21', '2020-04-27 04:30:21'),
(94, 'edit_social_networks', 'social_networks', '2020-04-27 04:30:21', '2020-04-27 04:30:21'),
(95, 'add_social_networks', 'social_networks', '2020-04-27 04:30:21', '2020-04-27 04:30:21'),
(96, 'delete_social_networks', 'social_networks', '2020-04-27 04:30:21', '2020-04-27 04:30:21');

-- --------------------------------------------------------

--
-- Структура таблицы `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-03-27 01:51:24', '2020-03-27 01:51:24'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-03-27 01:51:24', '2020-03-27 01:51:24'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-03-27 01:51:24', '2020-03-27 01:51:24'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-03-27 01:51:24', '2020-03-27 01:51:24');

-- --------------------------------------------------------

--
-- Структура таблицы `requests`
--

CREATE TABLE `requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` int(11) DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-03-27 01:51:20', '2020-03-27 01:51:20'),
(2, 'user', 'Normal User', '2020-03-27 01:51:20', '2020-03-27 01:51:20');

-- --------------------------------------------------------

--
-- Структура таблицы `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Буква закона', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Буква закона', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 8, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin'),
(12, 'site.mailerRequest', 'Почта для заявки', NULL, NULL, 'text', 4, 'Site'),
(13, 'site.phone', 'Телефон', NULL, NULL, 'text', 6, 'Site'),
(14, 'site.address', 'Адрес', NULL, NULL, 'text', 7, 'Site');

-- --------------------------------------------------------

--
-- Структура таблицы `social_networks`
--

CREATE TABLE `social_networks` (
  `id` int(10) UNSIGNED NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2020-03-27 01:51:25', '2020-03-27 01:51:25'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2020-03-27 01:51:25', '2020-03-27 01:51:25');

-- --------------------------------------------------------

--
-- Структура таблицы `treaties`
--

CREATE TABLE `treaties` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$lI..BHbLXI7oVUANjTWtNezYLef.BPnFxitbY6p/3No0.oy4Ukn8q', 'TsqVuJZnnK2hkjZylKi1O5YKe7OeVZYjrsTlkHIjlknGQUJ0rAuQzTsQEdBD', '{\"locale\":\"ru\"}', '2020-03-27 01:51:24', '2020-03-27 02:14:37');

-- --------------------------------------------------------

--
-- Структура таблицы `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `advantages`
--
ALTER TABLE `advantages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `brochures`
--
ALTER TABLE `brochures`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Индексы таблицы `components`
--
ALTER TABLE `components`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Индексы таблицы `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Индексы таблицы `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Индексы таблицы `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Индексы таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Индексы таблицы `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Индексы таблицы `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Индексы таблицы `social_networks`
--
ALTER TABLE `social_networks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Индексы таблицы `treaties`
--
ALTER TABLE `treaties`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Индексы таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `about`
--
ALTER TABLE `about`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `advantages`
--
ALTER TABLE `advantages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `brochures`
--
ALTER TABLE `brochures`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `components`
--
ALTER TABLE `components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT для таблицы `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `requests`
--
ALTER TABLE `requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `social_networks`
--
ALTER TABLE `social_networks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `treaties`
--
ALTER TABLE `treaties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Ограничения внешнего ключа таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
